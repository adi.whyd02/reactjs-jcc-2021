// soal 1

console.log("----- soal 1 -----");

const hitungLuasLingkaran = (r) => {
  return (22 / 7) * r * r;
};

const hitungKelilingLingkaran = (r) => {
  return ((2 * 22) / 7) * r;
};

let r = 7;
const luasLingkaran = hitungLuasLingkaran(r);
const kelilingLingkaran = hitungKelilingLingkaran(r);

console.log(luasLingkaran);
console.log(kelilingLingkaran);

// soal 2

console.log("----- soal 2 -----");

const introduce = (...rest) => {
  let [name, age, gender, profession] = rest;

  let greeting = gender === "Laki-Laki" ? "Pak" : "Bu";
  const word = `${greeting} ${name} adalah seorang ${profession} yang berusia ${age} tahun`;
  return word;
};

//kode di bawah ini jangan dirubah atau dihapus
const perkenalan = introduce("John", "30", "Laki-Laki", "penulis");
console.log(perkenalan);

// soal 3

console.log("----- soal 3 -----");

const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName() {
      console.log(firstName + " " + lastName);
    },
  };
};

// kode di bawah ini jangan diubah atau dihapus sama sekali
console.log(newFunction("John", "Doe").firstName);
console.log(newFunction("Richard", "Roe").lastName);
newFunction("William", "Imoh").fullName();

// soal 4

console.log("----- soal 4 -----");

let phone = {
  name: "Galaxy Note 20",
  brand: "Samsung",
  year: 2020,
  colors: ["Mystic Bronze", "Mystic White", "Mystic Black"],
};
// kode diatas ini jangan di rubah atau di hapus sama sekali

const { brand: phoneBrand, name: phoneName, year, colors } = phone;
const [colorBronze, colorWhite, colorBlack] = colors;

// kode di bawah ini jangan dirubah atau dihapus
console.log(phoneBrand, phoneName, year, colorBlack, colorBronze);

// soal 5

console.log("----- soal 5 -----");

let warna = ["biru", "merah", "kuning", "hijau"];

let dataBukuTambahan = {
  penulis: "john doe",
  tahunTerbit: 2020,
};

let buku = {
  nama: "pemograman dasar",
  jumlahHalaman: 172,
  warnaSampul: ["hitam"],
};
// kode diatas ini jangan di rubah atau di hapus sama sekali

let mergeWarnaSampul = {
  ...buku,
  warnaSampul: [...buku.warnaSampul, ...warna],
};
let mergeDataBukuTambahan = { ...mergeWarnaSampul, ...dataBukuTambahan };

console.log(mergeDataBukuTambahan);
