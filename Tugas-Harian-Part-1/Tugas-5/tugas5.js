// soal 1

console.log("----- soal 1 -----");

function luasPersegiPanjang(p, l) {
  var hitung = p * l;
  return hitung;
}

function kelilingPersegiPanjang(p, l) {
  var hitung = 2 * (p + l);
  return hitung;
}

function volumeBalok(p, l, t) {
  var hitung = p * l * t;
  return hitung;
}

var panjang = 12;
var lebar = 4;
var tinggi = 8;

var luasPersegiPanjang = luasPersegiPanjang(panjang, lebar);
var kelilingPersegiPanjang = kelilingPersegiPanjang(panjang, lebar);
var volumeBalok = volumeBalok(panjang, lebar, tinggi);

console.log(luasPersegiPanjang);
console.log(kelilingPersegiPanjang);
console.log(volumeBalok);

// soal 2

console.log("----- soal 2 -----");

function introduce(name, age, address, hobby) {
  var word =
    "Nama saya " +
    name +
    ", umur saya " +
    age +
    " tahun, alamat saya di " +
    address +
    ", dan saya punya hobby yaitu " +
    hobby +
    "!";
  return word;
}

var name = "John";
var age = 30;
var address = "Jalan belum jadi";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);

// soal 3

console.log("----- soal 3 -----");

var arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku", 1992];

var daftarPeserta = {
  nama: arrayDaftarPeserta[0],
  jenisKelamin: arrayDaftarPeserta[1],
  hobi: arrayDaftarPeserta[2],
  tahunLahir: arrayDaftarPeserta[3],
};

console.log(daftarPeserta);

// soal 4

console.log("----- soal 4 -----");

var fruits = [];

fruits.push(
  {
    nama: "Nanas",
    warna: "Kuning",
    adaBijinya: false,
    harga: 9000,
  },
  {
    nama: "Jeruk",
    warna: "Oranye",
    adaBijinya: true,
    harga: 8000,
  },
  {
    nama: "Semangka",
    warna: "Hijau & Merah",
    adaBijinya: true,
    harga: 10000,
  },
  {
    nama: "Pisang",
    warna: "Kuning",
    adaBijinya: false,
    harga: 5000,
  }
);

var buahTidakAdaBiji = fruits.filter(function (item) {
  return item.adaBijinya === false;
});

console.log(buahTidakAdaBiji);

// soal 5

console.log("----- soal 5 -----");

function tambahDataFilm(nama, durasi, genre, tahun) {
  dataFilm.push({
    nama: nama,
    durasi: durasi,
    genre: genre,
    tahun: tahun,
  });
}

var dataFilm = [];

tambahDataFilm("LOTR", "2 jam", "action", "1999");
tambahDataFilm("avenger", "2 jam", "action", "2019");
tambahDataFilm("spiderman", "2 jam", "action", "2004");
tambahDataFilm("juon", "2 jam", "horror", "2004");

console.log(dataFilm);
