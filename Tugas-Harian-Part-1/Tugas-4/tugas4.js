// soal 1

console.log("LOOPING PERTAMA");
var a = 1;
while (a <= 20) {
  if (a % 2 === 0) console.log(a + " - I love coding");
  a++;
}

console.log("LOOPING KEDUA");
var b = 20;
while (b >= 1) {
  if (b % 2 === 0) console.log(b + " - I will become a frontend developer");
  b--;
}

// soal 2

for (var c = 1; c <= 20; c++) {
  if (c % 2 !== 0 && c % 3 === 0) console.log(c + " - I Love Coding");
  else if (c % 2 !== 0) console.log(c + " - Santai");
  else console.log(c + " - Berkualitas");
}

//soal 3

var tampung = "";
for (var x = 0; x < 7; x++) {
  for (var y = 0; y <= x; y++) {
    tampung += "#";
  }
  tampung += "\n";
}
console.log(tampung);

// soal 4

var kalimat = [
  "aku",
  "saya",
  "sangat",
  "sangat",
  "senang",
  "belajar",
  "javascript",
];

kalimat.shift();
kalimat.splice(1, 1);
var result = kalimat.join(" ");

console.log(result);

// soal 5

var sayuran = [];

sayuran.push(
  "Kangkung",
  "Bayam",
  "Buncis",
  "Kubis",
  "Timun",
  "Seledri",
  "Tauge"
);
sayuran.sort();

for (var i = 0; i < sayuran.length; i++) {
  var no = i + 1;
  console.log(no + ". " + sayuran[i]);
}
