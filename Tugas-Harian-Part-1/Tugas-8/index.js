// di index.js
var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
  { name: "komik", timeSpent: 1000 },
];

// Tulis code untuk memanggil function readBooks di sini

const read = (time, books, i = 0) => {
  if (i < books.length) {
    readBooks(time, books[i], (sisa) => {
      if (sisa > 0) {
        i += 1;
        read(sisa, books, i);
      }
    });
  }
};

read(10000, books, 0);
