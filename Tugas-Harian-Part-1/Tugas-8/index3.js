var filterBooksPromise = require("./promise2.js");

// Lanjutkan code untuk menjalankan function filterBookProm

const filterBooks = async () => {
  filterBooksPromise(true, 40)
    .then((res) => {
      console.log(res);
    })
    .catch((err) => {
      console.log(err.message);
    });

  try {
    const x = await filterBooksPromise(false, 250);
    console.log(x);

    const y = await filterBooksPromise(true, 30);
    console.log(y);
  } catch (error) {
    console.log(error.message);
  }
};

filterBooks();
