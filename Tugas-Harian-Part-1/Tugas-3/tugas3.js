// soal 1

var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var gabungan = kataPertama.concat(
  " " + kataKedua.charAt(0).toUpperCase() + kataKedua.slice(1) + " ",
  kataKetiga.slice(0, 6) + kataKetiga.charAt(6).toUpperCase() + " ",
  kataKeempat.toUpperCase()
);

console.log("soal 1 | gabungan kata: ", gabungan);

// soal 2

var panjangPersegiPanjang = "8";
var lebarPersegiPanjang = "5";

var alasSegitiga = "6";
var tinggiSegitiga = "7";

var kelilingPersegiPanjang =
  2 * (parseInt(panjangPersegiPanjang) + parseInt(lebarPersegiPanjang));
var luasSegitiga = 0.5 * parseInt(alasSegitiga) * parseInt(tinggiSegitiga);

console.log("soal 2 | keliling Persegi Panjang: ", kelilingPersegiPanjang);
console.log("soal 2 | luas Segitiga: ", luasSegitiga);

// soal 3

var sentences = "wah javascript itu keren sekali";

var firstWord = sentences.substring(0, 3);
var secondWord = sentences.substring(4, 14);
var thirdWord = sentences.substring(15, 18);
var fourthWord = sentences.substring(19, 24);
var fifthWord = sentences.substring(25, 32);

console.log("soal 3 | Kata Pertama: " + firstWord);
console.log("soal 3 | Kata Kedua: " + secondWord);
console.log("soal 3 | Kata Ketiga: " + thirdWord);
console.log("soal 3 | Kata Keempat: " + fourthWord);
console.log("soal 3 | Kata Kelima: " + fifthWord);

// soal 4

var nilaiJohn = 80;

if (nilaiJohn >= 80) {
  console.log("soal 4 | John: A");
} else if (nilaiJohn >= 70) {
  console.log("soal 4 | John: B");
} else if (nilaiJohn >= 60) {
  console.log("soal 4 | John: C");
} else if (nilaiJohn >= 50) {
  console.log("soal 4 | John: D");
} else if (nilaiJohn < 50) {
  console.log("soal 4 | John: E");
} else {
  console.log("Nilai tidak terindex");
}

var nilaiDoe = 50;

if (nilaiDoe >= 80) {
  console.log("soal 4 | Doe: A");
} else if (nilaiDoe >= 70) {
  console.log("soal 4 | Doe: B");
} else if (nilaiDoe >= 60) {
  console.log("soal 4 | Doe: C");
} else if (nilaiDoe >= 50) {
  console.log("soal 4 | Doe: D");
} else if (nilaiDoe < 50) {
  console.log("soal 4 | Doe: E");
} else {
  console.log("Nilai tidak terindex");
}

// soal 5

var tanggal = 2;
var bulan = 9;
var tahun = 2002;

var namaBulan;
switch (bulan) {
  case 1:
    namaBulan = "Januari";
    break;
  case 2:
    namaBulan = "Februari";
    break;
  case 3:
    namaBulan = "Maret";
    break;
  case 4:
    namaBulan = "April";
    break;
  case 5:
    namaBulan = "Mei";
    break;
  case 6:
    namaBulan = "Juni";
    break;
  case 7:
    namaBulan = "Juli";
    break;
  case 8:
    namaBulan = "Agustus";
    break;
  case 9:
    namaBulan = "September";
    break;
  case 10:
    namaBulan = "Oktober";
    break;
  case 11:
    namaBulan = "November";
    break;
  case 12:
    namaBulan = "Desember";
    break;
  default:
    namaBulan = "Range bulan hanya 1-12";
    break;
}

var lahirSaya = tanggal + " " + namaBulan + " " + tahun;
console.log("soal 5 | lahir saya: ", lahirSaya);
