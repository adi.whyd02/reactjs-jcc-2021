import { Switch, Route, Redirect } from "react-router-dom";
import { MoviesProvider } from "../context/movies-context";
import { GamesProvider } from "../context/games-context";
import ChangePassword from "../pages/auth/ChangePassword";
import Login from "../pages/auth/Login";
import Register from "../pages/auth/Register";
import Dashboard from "../pages/dashboard/Dashboard";
import Games from "../pages/games/Games";
import GamesForm from "../pages/games/GamesForm";
import GamesList from "../pages/games/GamesList";
import Home from "../pages/home/Home";
import Movies from "../pages/movies/Movies";
import MoviesForm from "../pages/movies/MoviesForm";
import MoviesList from "../pages/movies/MoviesList";
import Cookies from "js-cookie";
import MoviesDetail from "../pages/movies/MoviesDetail";
import GamesDetail from "../pages/games/GamesDetail";

const MainRoute = () => {
  function PrivateRoute({ children, ...rest }) {
    return (
      <Route
        {...rest}
        render={({ location }) =>
          Cookies.get("token") ? (
            children
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: location },
              }}
            />
          )
        }
      />
    );
  }

  return (
    <Switch>
      <Route exact path="/login">
        <Login />
      </Route>
      <Route exact path="/register">
        <Register />
      </Route>
      <PrivateRoute exact path="/change-password">
        <ChangePassword />
      </PrivateRoute>

      <MoviesProvider>
        <GamesProvider>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/movies">
            <Movies />
          </Route>
          <Route path="/movies/detail/:id">
            <MoviesDetail />
          </Route>
          <Route exact path="/games">
            <Games />
          </Route>
          <Route path="/games/detail/:id">
            <GamesDetail />
          </Route>
          <PrivateRoute exact path="/dashboard">
            <Dashboard />
          </PrivateRoute>
          <PrivateRoute exact path="/movies-list">
            <MoviesList />
          </PrivateRoute>
          <PrivateRoute exact path="/movies-list/create">
            <MoviesForm />
          </PrivateRoute>
          <PrivateRoute exact path="/movies-list/edit/:id">
            <MoviesForm />
          </PrivateRoute>
          <PrivateRoute exact path="/games-list">
            <GamesList />
          </PrivateRoute>
          <PrivateRoute exact path="/games-list/create">
            <GamesForm />
          </PrivateRoute>
          <PrivateRoute exact path="/games-list/edit/:id">
            <GamesForm />
          </PrivateRoute>
        </GamesProvider>
      </MoviesProvider>
    </Switch>
  );
};

export default MainRoute;
