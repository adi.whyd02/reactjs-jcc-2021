import React, { createContext, useState } from "react";
import axios from "axios";
import { message } from "antd";
import Cookies from "js-cookie";

export const GamesContext = createContext();

export const GamesProvider = (props) => {
  const [dataGames, setDataGames] = useState([]);
  const [currentId, setCurrentId] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const headers = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  const api = {
    handleGetAll: async () => {
      try {
        setIsLoading(true);
        const result = await axios.get(
          `https://backendexample.sanbersy.com/api/data-game`
        );

        setDataGames(
          result.data.map((item, index) => {
            return {
              no: index + 1,
              id: item.id,
              name: item.name,
              release: item.release,
              platform: item.platform,
              genre: item.genre,
              singlePlayer: item.singlePlayer,
              multiplayer: item.multiplayer,
              isSinglePlayer: item.singlePlayer ? "Yes" : "No",
              isMultiPlayer: item.multiplayer ? "Yes" : "No",
              image_url: item.image_url,
            };
          })
        );
        setIsLoading(false);

        return dataGames;
      } catch (error) {
        message.error(`Data failed to load, ${error.message}`);
        setIsLoading(false);
      }
    },
    handleCreate: (input) => {
      setIsLoading(true);

      axios
        .post(
          `https://backendexample.sanbersy.com/api/data-game`,
          {
            ...input,
            singlePlayer: input.player.includes("singlePlayer"),
            multiplayer: input.player.includes("multiPlayer"),
          },
          headers
        )
        .then(() => {
          api.handleGetAll();
          setTimeout(() => {
            message.success("Data berhasil di tambahkan!");
          }, 1000);
        })
        .catch((error) =>
          message.error(`Data gagal ditambahkan, ${error.message}`)
        );
      setIsLoading(false);
    },
    handleRead: async (id) => {
      try {
        setIsLoading(true);
        const load = await axios.get(
          `https://backendexample.sanbersy.com/api/data-game/${id}`
        );
        setIsLoading(false);

        return load.data;
      } catch (error) {
        setIsLoading(false);

        message.error(`Data failed to load, ${error.message}`);
      }
    },
    handleUpdate: (input) => {
      setIsLoading(true);

      axios
        .put(
          `https://backendexample.sanbersy.com/api/data-game/${currentId}`,
          {
            ...input,
            singlePlayer: input.player.includes("singlePlayer"),
            multiplayer: input.player.includes("multiPlayer"),
          },
          headers
        )
        .then(() => {
          api.handleGetAll();
          setTimeout(() => {
            message.success("Data berhasil di update!");
          }, 1000);
        })
        .catch((error) =>
          message.error(`Data gagal di update, ${error.message}`)
        );
      setIsLoading(false);
    },
    handleDelete: (id) => {
      setIsLoading(true);
      axios
        .delete(
          `https://backendexample.sanbersy.com/api/data-game/${id}`,
          headers
        )
        .then(() => {
          api.handleGetAll();
          message.success("Data berhasil di hapus!");
        })
        .catch((error) =>
          message.error(`Data gagal dihapus, ${error.message}`)
        );
      setIsLoading(false);
    },
  };

  return (
    <GamesContext.Provider
      value={{
        dataGames,
        setDataGames,
        currentId,
        setCurrentId,
        isLoading,
        api,
      }}
    >
      {props.children}
    </GamesContext.Provider>
  );
};
