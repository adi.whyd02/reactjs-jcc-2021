import React, { createContext, useState } from "react";
import axios from "axios";
import { message } from "antd";
import Cookies from "js-cookie";

export const MoviesContext = createContext();

export const MoviesProvider = (props) => {
  const [dataMovies, setDataMovies] = useState([]);
  const [currentId, setCurrentId] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const headers = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  const api = {
    handleGetAll: async () => {
      try {
        setIsLoading(true);
        const result = await axios.get(
          `https://backendexample.sanbersy.com/api/data-movie`
        );

        setDataMovies(
          result.data.map((item, index) => {
            return {
              no: index + 1,
              id: item.id,
              title: item.title,
              description: item.description,
              year: item.year,
              duration: item.duration,
              genre: item.genre,
              rating: item.rating,
              review: item.review,
              image_url: item.image_url,
            };
          })
        );
        setIsLoading(false);

        return dataMovies;
      } catch (error) {
        message.error(`Data failed to load, ${error.message}`);
        setIsLoading(false);
      }
    },
    handleCreate: (input) => {
      setIsLoading(true);

      axios
        .post(
          `https://backendexample.sanbersy.com/api/data-movie`,
          input,
          headers
        )
        .then(() => {
          api.handleGetAll();
          setTimeout(() => {
            message.success("Data berhasil di tambahkan!");
          }, 1000);
        })
        .catch((error) =>
          message.error(`Data gagal ditambahkan, ${error.message}`)
        );
      setIsLoading(false);
    },
    handleRead: async (id) => {
      try {
        setIsLoading(true);
        const load = await axios.get(
          `https://backendexample.sanbersy.com/api/data-movie/${id}`
        );
        setIsLoading(false);

        return load.data;
      } catch (error) {
        message.error(`Data failed to load, ${error.message}`);
        setIsLoading(false);
      }
    },
    handleUpdate: (input) => {
      setIsLoading(true);

      axios
        .put(
          `https://backendexample.sanbersy.com/api/data-movie/${currentId}`,
          input,
          headers
        )
        .then(() => {
          api.handleGetAll();
          setTimeout(() => {
            message.success("Data berhasil di update!");
          }, 1000);
        })
        .catch((error) =>
          message.error(`Data gagal di update, ${error.message}`)
        );
      setIsLoading(false);
    },
    handleDelete: (id) => {
      setIsLoading(true);

      axios
        .delete(
          `https://backendexample.sanbersy.com/api/data-movie/${id}`,
          headers
        )
        .then(() => {
          api.handleGetAll();
          message.success("Data berhasil di hapus!");
        })
        .catch((error) =>
          message.error(`Data gagal dihapus, ${error.message}`)
        );
      setIsLoading(false);
    },
  };

  return (
    <MoviesContext.Provider
      value={{
        dataMovies,
        setDataMovies,
        currentId,
        setCurrentId,
        isLoading,
        api,
      }}
    >
      {props.children}
    </MoviesContext.Provider>
  );
};
