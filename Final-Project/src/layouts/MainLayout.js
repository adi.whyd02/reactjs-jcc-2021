import { Layout } from "antd";
import { useState } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import Section from "../components/Section";
import Footer from "../components/Footer";
import Header from "../components/Header";
import Sidebar from "../components/Sidebar";
import MainRoute from "../routes/MainRoute";

const MainLayout = () => {
  const [conditionSidebar, setConditionSidebar] = useState(true);

  return (
    <Layout>
      <Router>
        <Sidebar isActive={[conditionSidebar, setConditionSidebar]} />
        <Layout>
          <Header isActive={[conditionSidebar, setConditionSidebar]} />
          <Section>
            <MainRoute />
          </Section>
          <Footer />
        </Layout>
      </Router>
    </Layout>
  );
};

export default MainLayout;
