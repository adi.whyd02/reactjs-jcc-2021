import { Skeleton } from "antd";

const CardSkeleton = () => {
  const items = [1, 2, 3];

  return (
    <div className="flex flex-wrap">
      {items.map((item) => (
        <div
          key={item}
          className="w-full sm:w-1/2 md:w-1/2 xl:w-1/4 h-96 p-4 bg-gray-200 rounded-xl mr-5 mb-5"
        >
          <Skeleton.Image size="large" />
          <Skeleton />
        </div>
      ))}
    </div>
  );
};

export default CardSkeleton;
