import { Layout } from "antd";
import { useLocation } from "react-router";
const { Footer } = Layout;

const MyFooter = () => {
  const location = useLocation();
  return (
    <>
      {location.pathname.toLocaleLowerCase() !== "/login" &&
      location.pathname.toLocaleLowerCase() !== "/register" ? (
        <Footer className="w-full bottom-0 text-center text-xs">
          &copy; 2021 JCC Created By Adi Wahyudi
        </Footer>
      ) : (
        <></>
      )}
    </>
  );
};

export default MyFooter;
