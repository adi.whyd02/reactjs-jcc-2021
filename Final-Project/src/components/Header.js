import {
  UserOutlined,
  SearchOutlined,
  BarsOutlined,
  SlackOutlined,
  CloseOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { useState } from "react";
import { Link, NavLink, useLocation, useHistory } from "react-router-dom";
import Cookies from "js-cookie";
import { Dropdown, Menu } from "antd";

const Header = (props) => {
  const [conditionSidebar, setConditionSidebar] = props.isActive;
  const [search, setSearch] = useState("");
  const [filtered, setFiltered] = useState([]);
  const link = [
    {
      key: "Home",
      description: "Top rated movie, Most recent movie, Most recent game",
      path: "/",
      isLogin: false,
    },
    {
      key: "Movies",
      description: "All movies",
      path: "/movies",
      isLogin: false,
    },
    { key: "Games", description: "All games", path: "/games", isLogin: false },
    {
      key: "Dashboard",
      description: "Statistic data",
      path: "/dashboard",
      isLogin: true,
    },
    {
      key: "Manage Movie",
      description:
        "Manage movie list, Add movie, Edit movie, Delete movie, Filter movie",
      path: "/movies-list",
      isLogin: true,
    },
    {
      key: "Add new Movie",
      description: "Movie form create",
      path: "/movies-list/create",
      isLogin: true,
    },
    {
      key: "Manage game",
      description: "Manage game list, Add game, Edit game, Delete game",
      path: "/games-list",
      isLogin: true,
    },
    {
      key: "Add new Game",
      description: "Game form create",
      path: "/games-list/create",
      isLogin: true,
    },
    {
      key: "Change Password",
      description: "Change fassword form",
      path: "/change-password",
      isLogin: true,
    },
  ];

  const handleSearch = (e) => {
    let value = e.target.value;
    setSearch(value);

    if (value) {
      const cekUser = Cookies.get("token") ? true : false;

      const filter = link.filter((item) => {
        let cekFilter =
          item.key.toLowerCase().indexOf(value.toLowerCase()) >= 0 ||
          item.description.toLowerCase().indexOf(value.toLowerCase()) >= 0;

        return item.isLogin === cekUser && cekFilter;
      });
      setFiltered(filter);
    } else {
      setFiltered([]);
    }
  };

  const location = useLocation();
  const history = useHistory();

  const logout = () => {
    Cookies.remove("token");
    Cookies.remove("name");
    Cookies.remove("email");
    history.push("/login");
  };

  const ddprofil = (
    <Menu>
      <Menu.Item key="logout" onClick={logout} icon={<LogoutOutlined />}>
        <span>Logout</span>
      </Menu.Item>
    </Menu>
  );

  return (
    <>
      {location.pathname.toLocaleLowerCase() !== "/login" &&
      location.pathname.toLocaleLowerCase() !== "/register" ? (
        <div className="w-full flex justify-between items-center px-5 py-4 border-b-2">
          <div className="flex w-1/3 justify-between space-x-20 items-center px-12">
            {Cookies.get("token") && (
              <div
                onClick={() => setConditionSidebar(!conditionSidebar)}
                className="flex-shrink-0 bg-white w-8 h-8 rounded-xl flex items-center justify-center text-indigo-700 text-xl font-bold mb-1 cursor-pointer"
              >
                <BarsOutlined />
              </div>
            )}

            {(!conditionSidebar || !Cookies.get("token")) && (
              <div className="flex justify-center items-center space-x-3">
                <p className="text-center text-lg font-bold text-indigo-800 font-sans">
                  <SlackOutlined />
                </p>
                <p className="text-center text-lg font-bold mt-2 text-indigo-800 font-sans">
                  Movgem
                </p>
              </div>
            )}
            <div>
              {!conditionSidebar}
              <div className="bg-white p-2 rounded-full flex items-center space-x-2 px-5">
                <SearchOutlined style={{ color: "rgba(196, 181, 253, 400)" }} />
                <input
                  type="text"
                  placeholder="Search"
                  className="w-60 px-2 text-indigo-700 outline-none"
                  value={search}
                  onChange={handleSearch}
                />
              </div>
              {search && (
                <div className="w-96 bg-white rounded-lg shadow-md p-5 absolute z-10 mt-3">
                  <div className="flex justify-between items-center border-b-2 pb-4 mb-4">
                    <p className="mb-0 text-indigo-700 font-bold">Link</p>
                    <CloseOutlined onClick={() => setSearch("")} />
                  </div>

                  {filtered.slice(0, 3).map((item) => (
                    <Link to={item.path} onClick={() => setSearch("")}>
                      <div className="border-b-2 mb-5 hover:bg-gray-50 p-3 rounded-md">
                        <p className="text-indigo-700">{item.key}</p>
                        <p className="text-xs text-gray-600">
                          {item.description}
                        </p>
                      </div>
                    </Link>
                  ))}
                  {!filtered.length && <p className="mb-0">No result</p>}
                </div>
              )}
            </div>
          </div>
          <div className="flex justify-end items-center space-x-20">
            <div className="flex space-x-12">
              <NavLink
                to="/"
                exact
                activeClassName="selected-navbar"
                className="text-gray-500 text-base font-bold hover:text-indigo-700"
              >
                Home
              </NavLink>
              <NavLink
                to="/movies"
                activeClassName="selected-navbar"
                className="text-gray-500 text-base font-bold hover:text-indigo-700"
              >
                Movies
              </NavLink>
              <NavLink
                to="/games"
                activeClassName="selected-navbar"
                className="text-gray-500 text-base font-bold hover:text-indigo-700"
              >
                Games
              </NavLink>
            </div>
            {Cookies.get("token") ? (
              <Dropdown overlay={ddprofil}>
                <div className="bg-white w-min-40 p-2 px-5 rounded-lg flex items-center space-x-5">
                  <div className="flex justify-center items-center bg-indigo-500 text-white rounded-full h-8 w-8">
                    <UserOutlined />
                  </div>
                  <p className="text-indigo-500 font-bold mb-0">
                    {Cookies.get("name")}
                  </p>
                </div>
              </Dropdown>
            ) : (
              <div className="flex space-x-2">
                <Link to="/login">
                  <div className="bg-white w-min-40 p-2 px-5 rounded-lg flex items-center space-x-5">
                    <p className="text-indigo-500 font-bold mb-0">Login</p>
                  </div>
                </Link>
                <Link to="/register">
                  <div className="bg-indigo-500 w-min-40 p-2 px-5 rounded-lg flex items-center space-x-5">
                    <p className="text-white font-bold mb-0">Register</p>
                  </div>
                </Link>
              </div>
            )}
          </div>
        </div>
      ) : (
        <></>
      )}
    </>
  );
};

export default Header;
