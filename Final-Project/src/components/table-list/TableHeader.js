import { Link } from "react-router-dom";
import { Badge } from "antd";

import { SearchOutlined, FilterOutlined } from "@ant-design/icons";

import MoviesModalFilter from "../movies/MoviesModalFilter";
import GamesModalFilter from "../games/GamesModalFilter";
import TabPaneHeader from "./TabPaneHeader";

const TableHeader = (props) => {
  const { name } = props;

  const { setVisibleModal, isFiltered } = props.filterFeature;
  const { search, handleSearch } = props.searchFeature;

  return (
    <>
      <div className="flex justify-between items-center pb-5 mb-3">
        <h1 className="text-lg font-bold mb-0">List of {name}</h1>
        <Link to={`/${name}-list/create`}>
          <button className="bg-indigo-200 text-indigo-700 font-bold hover:bg-indigo-500 hover:text-white px-7 py-2 rounded-md">
            Add new {name}
          </button>
        </Link>
      </div>
      <TabPaneHeader tabFeature={props.tabFeature} />
      <div className="my-5 flex justify-between items-center">
        <div className="w-80 bg-gray-100 p-2 rounded-md flex items-center space-x-2 px-5">
          <SearchOutlined style={{ color: "rgba(196, 181, 253, 400)" }} />
          <input
            type="text"
            placeholder={
              name === "movies" ? "Search by title" : "Search by name"
            }
            className="w-60 px-2 text-indigo-700 outline-none bg-transparent"
            value={search}
            onChange={handleSearch}
          />
        </div>
        <div>
          <Badge dot={isFiltered}>
            <div
              onClick={() => setVisibleModal(true)}
              title="Filter"
              className="bg-gray-100 p-3 rounded-md flex items-center font-bold text-indigo-700 cursor-pointer hover:bg-indigo-500 hover:text-white"
            >
              <FilterOutlined />
            </div>
          </Badge>
          {name === "movies" ? (
            <MoviesModalFilter filterFeature={props.filterFeature} />
          ) : (
            <GamesModalFilter filterFeature={props.filterFeature} />
          )}
        </div>
      </div>
    </>
  );
};

export default TableHeader;
