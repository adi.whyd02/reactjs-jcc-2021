import { Tabs } from "antd";
const { TabPane } = Tabs;

const TabPaneHeader = (props) => {
  const { tabList, handleChangeTab } = props.tabFeature;
  return (
    <Tabs defaultActiveKey="All" onChange={handleChangeTab}>
      {tabList.map((item) => (
        <TabPane tab={item} key={item} />
      ))}
    </Tabs>
  );
};

export default TabPaneHeader;
