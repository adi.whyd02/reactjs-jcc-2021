import { Button, Modal, Form, InputNumber, Select } from "antd";

const { Option } = Select;

const MoviesModalFilter = (props) => {
  const { visibleModal, setVisibleModal, form, handleFilter, handleReset } =
    props.filterFeature;
  return (
    <>
      {visibleModal}
      <Modal
        centered
        visible={visibleModal}
        title="Filter Movies"
        onOk={handleFilter}
        onCancel={() => setVisibleModal(false)}
        footer={null}
        getContainer={false}
      >
        <Form layout="vertical" form={form} onFinish={handleFilter}>
          <div className="flex space-x-5 flex-wrap">
            <Form.Item name="rating" label="Rating">
              <Select style={{ width: 140 }} allowClear>
                <Option value="0">0</Option>
                <Option value="1">1</Option>
                <Option value="2">2</Option>
                <Option value="3">3</Option>
                <Option value="4">4</Option>
                <Option value="5">5</Option>
                <Option value="6">6</Option>
                <Option value="7">7</Option>
                <Option value="8">8</Option>
                <Option value="9">9</Option>
                <Option value="10">10</Option>
              </Select>
            </Form.Item>
            <Form.Item name="year" label="Year">
              <InputNumber min="1980" max="2021" style={{ width: 140 }} />
            </Form.Item>
            <Form.Item name="duration" label="Max Duration">
              <InputNumber style={{ width: 140 }} />
            </Form.Item>
          </div>
          <div className="w-full flex justify-between items-center mt-7">
            <Form.Item>
              <Button key="reset" onClick={handleReset}>
                Reset
              </Button>
            </Form.Item>
            <Form.Item>
              <Button
                key="submit"
                style={{
                  background: "#6366F1",
                  color: "white",
                  padding: "0 45px",
                  marginRight: "10px",
                }}
                htmlType="submit"
              >
                Filter
              </Button>
            </Form.Item>
          </div>
        </Form>
      </Modal>
    </>
  );
};

export default MoviesModalFilter;
