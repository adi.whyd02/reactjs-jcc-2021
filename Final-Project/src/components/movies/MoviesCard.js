import { ClockCircleOutlined, AlignCenterOutlined } from "@ant-design/icons";
import { Rate } from "antd";
import { Link } from "react-router-dom";
import CardSkeleton from "../skeloton/CardSkeleton";

const MoviesCard = (props) => {
  return (
    <>
      <div className="flex flex-wrap">
        {!props.isLoading &&
          props.data.map((item) => (
            <div
              key={item.id}
              className="w-full sm:w-1/2 md:w-1/2 xl:w-1/4 p-4"
            >
              <Link
                to={`/movies/detail/${item.id}`}
                className="c-card block bg-white shadow-md hover:shadow-xl rounded-lg overflow-hidden"
              >
                <div className="relative pb-80 overflow-hidden">
                  <img
                    className="absolute inset-0 h-full w-full object-cover"
                    src={item.image_url}
                    alt={item.image_url}
                  />
                </div>
                <div className="p-4">
                  <h2 className="mt-2 mb-2  font-bold">{item.title}</h2>
                  <p className="text-xs text-gray-500">{item.genre}</p>
                </div>
                <div className="p-4 border-t border-b text-xs text-gray-700 flex justify-between">
                  <span className="flex items-center text-gray-900">
                    <AlignCenterOutlined className="mr-2" />
                    {item.year}
                  </span>
                  <span className="flex items-center mb-1 text-gray-900">
                    <ClockCircleOutlined className="mr-2" /> {item.duration}{" "}
                    minutes
                  </span>
                </div>
                <div className="p-4 flex items-center text-sm text-gray-600">
                  <Rate
                    disabled
                    value={item.rating}
                    count={10}
                    style={{ fontSize: "10pt" }}
                  />
                  <span className="ml-2 text-xs">{item.rating}/10</span>
                </div>
              </Link>
            </div>
          ))}
      </div>
      <div>{props.isLoading && <CardSkeleton />}</div>
    </>
  );
};

export default MoviesCard;
