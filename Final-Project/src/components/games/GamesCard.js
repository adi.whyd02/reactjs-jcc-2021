import { Link } from "react-router-dom";
import {
  NodeCollapseOutlined,
  AlignCenterOutlined,
  CheckCircleOutlined,
} from "@ant-design/icons";
import CardSkeleton from "../skeloton/CardSkeleton";
const GamesCard = (props) => {
  return (
    <>
      <div className="flex flex-wrap">
        {!props.isLoading &&
          props.data.map((item) => (
            <div
              key={item.id}
              className="w-full sm:w-1/2 md:w-1/2 xl:w-1/4 p-4"
            >
              <Link
                to={`/games/detail/${item.id}`}
                className="c-card block bg-white shadow-md hover:shadow-xl rounded-lg overflow-hidden"
              >
                <div className="relative pb-80 overflow-hidden">
                  <img
                    className="absolute inset-0 h-full w-full object-cover"
                    src={item.image_url}
                    alt={item.image_url}
                  />
                </div>
                <div className="p-4">
                  <h2 className="mt-2 mb-2  font-bold">{item.name}</h2>
                  <p className="text-xs text-gray-500">{item.genre}</p>
                </div>
                <div className="p-4 border-t border-b text-xs text-gray-700 flex justify-between flex-wrap">
                  <span className="flex items-center text-gray-900 my-2">
                    <AlignCenterOutlined className="mr-2" />
                    {item.release}
                  </span>
                  <span className="flex items-center mb-1 text-gray-900 my-2">
                    <NodeCollapseOutlined className="mr-2" /> {item.platform}
                  </span>
                </div>
                <div className="p-4 flex space-x-5 items-center text-sm">
                  {item.singlePlayer === 1 && (
                    <div className="flex justify-start space-x-2 items-center">
                      <span className="text-blue-500 text-xl">
                        <CheckCircleOutlined />
                      </span>
                      <span className="ml-2 text-xs text-black mt-2">
                        Single Player
                      </span>
                    </div>
                  )}

                  {item.multiplayer === 1 && (
                    <div className="flex justify-start space-x-2 items-center">
                      <span className="text-blue-500 text-xl">
                        <CheckCircleOutlined />
                      </span>
                      <span className="ml-2 text-xs text-black mt-2">
                        Multi Player
                      </span>
                    </div>
                  )}

                  {(item.multiplayer === 0 && item.singlePlayer === 0) ||
                    (!item.multiplayer && !item.singlePlayer && (
                      <span className="ml-2 text-xs text-black mt-2">
                        The number of players does not exist
                      </span>
                    ))}
                </div>
              </Link>
            </div>
          ))}
      </div>
      <div>{props.isLoading && <CardSkeleton />}</div>
    </>
  );
};

export default GamesCard;
