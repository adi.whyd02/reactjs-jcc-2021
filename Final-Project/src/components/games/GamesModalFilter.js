import { Button, Modal, Form, Input, InputNumber, Checkbox } from "antd";

const GamesModalFilter = (props) => {
  const { visibleModal, setVisibleModal, form, handleFilter, handleReset } =
    props.filterFeature;
  return (
    <>
      {visibleModal}
      <Modal
        centered
        visible={visibleModal}
        title="Filter Games"
        onOk={handleFilter}
        onCancel={() => setVisibleModal(false)}
        footer={null}
        getContainer={false}
      >
        <Form layout="vertical" form={form} onFinish={handleFilter}>
          <div className="flex space-x-5 flex-wrap">
            <Form.Item name="platform" label="Platform">
              <Input style={{ width: 140 }} />
            </Form.Item>
            <Form.Item name="release" label="Release">
              <InputNumber min="2000" max="2021" style={{ width: 140 }} />
            </Form.Item>
            <Form.Item name="player" label="Player" initialValue={[]}>
              <Checkbox.Group style={{ width: 140 }}>
                <div className="flex space-x-3">
                  <Checkbox value="singlePlayer">
                    <p className="text-xs mb-0">Single</p>
                  </Checkbox>
                  <Checkbox value="multiPlayer">
                    <p className="text-xs mb-0">Multi</p>
                  </Checkbox>
                </div>
              </Checkbox.Group>
            </Form.Item>
          </div>
          <div className="w-full flex justify-between items-center mt-7">
            <Form.Item>
              <Button key="reset" onClick={handleReset}>
                Reset
              </Button>
            </Form.Item>
            <Form.Item>
              <Button
                key="submit"
                style={{
                  background: "#6366F1",
                  color: "white",
                  padding: "0 45px",
                  marginRight: "10px",
                }}
                htmlType="submit"
              >
                Filter
              </Button>
            </Form.Item>
          </div>
        </Form>
      </Modal>
    </>
  );
};

export default GamesModalFilter;
