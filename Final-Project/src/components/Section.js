const Section = (props) => {
  return (
    <div className="flex justify-center mt-10 mb-32">
      <div className="container w-11/12">{props.children}</div>
    </div>
  );
};

export default Section;
