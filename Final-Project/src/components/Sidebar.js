import { Layout } from "antd";
import {
  SlackOutlined,
  AppstoreOutlined,
  SettingOutlined,
  PlusCircleOutlined,
  KeyOutlined,
} from "@ant-design/icons";
import { NavLink, useLocation } from "react-router-dom";
import Cookies from "js-cookie";

const { Sider } = Layout;

const Sidebar = (props) => {
  const [conditionSidebar] = props.isActive;
  const location = useLocation();

  return (
    <>
      {conditionSidebar &&
        location.pathname.toLocaleLowerCase() !== "/login" &&
        location.pathname.toLocaleLowerCase() !== "/register" &&
        Cookies.get("token") && (
          <Sider
            theme="light"
            width="250"
            style={{
              minHeight: "100vh",
            }}
          >
            <div
              className="px-10 pt-5"
              style={{
                overflow: "auto",
                width: "250px",
                height: "100vh",
                position: "fixed",
                left: 0,
              }}
            >
              <div className="flex justify-center items-center space-x-3">
                <p className="text-center text-lg font-bold text-indigo-800 font-sans">
                  <SlackOutlined />
                </p>
                <p className="text-center text-lg font-bold mt-2 text-indigo-800 font-sans">
                  Movgem
                </p>
              </div>
              <div className="mt-20">
                <div>
                  <NavLink
                    to="/dashboard"
                    activeClassName="selected-sidebar"
                    className="group text-decoration-none navigasi-sidebar"
                  >
                    <div className="transition duration-150 group-hover:bg-indigo-500 px-3 py-2 mt-1 rounded flex justify-start items-center">
                      <div className="text-indigo-700 group-hover:text-white mr-3 mb-2">
                        <AppstoreOutlined />
                      </div>
                      <p className="text-indigo-700 group-hover:text-white group-hover:font-bold text-sm">
                        Dashboard
                      </p>
                    </div>
                  </NavLink>

                  <div className="mb-4 mt-3">
                    <p
                      className="font-weight-bold"
                      style={{ color: "lightgrey", fontSize: "9pt" }}
                    >
                      Movie
                    </p>
                  </div>
                  <NavLink
                    to="/movies-list"
                    exact
                    activeClassName="selected-sidebar"
                    className="group text-decoration-none navigasi-sidebar"
                  >
                    <div className="transition duration-150 group-hover:bg-indigo-500 px-3 py-2 mt-1 rounded flex justify-start items-center">
                      <div className="text-indigo-700 group-hover:text-white mr-3 mb-2">
                        <SettingOutlined />
                      </div>
                      <p className="text-indigo-700 group-hover:text-white group-hover:font-bold text-sm">
                        Manage Movie
                      </p>
                    </div>
                  </NavLink>
                  <NavLink
                    to="/movies-list/create"
                    activeClassName="selected-sidebar"
                    className="group text-decoration-none navigasi-sidebar"
                  >
                    <div className="transition duration-150 group-hover:bg-indigo-500 px-3 py-2 mt-1 rounded flex justify-start items-center">
                      <div className="text-indigo-700 group-hover:text-white mr-3 mb-2">
                        <PlusCircleOutlined />
                      </div>
                      <p className="text-indigo-700 group-hover:text-white group-hover:font-bold text-sm">
                        Add Movie
                      </p>
                    </div>
                  </NavLink>
                  <div className="mb-4 mt-3">
                    <p
                      className="font-weight-bold"
                      style={{ color: "lightgrey", fontSize: "9pt" }}
                    >
                      Games
                    </p>
                  </div>
                  <NavLink
                    to="/games-list"
                    exact
                    activeClassName="selected-sidebar"
                    className="group text-decoration-none navigasi-sidebar"
                  >
                    <div className="transition duration-150 group-hover:bg-indigo-500 px-3 py-2 mt-1 rounded flex justify-start items-center">
                      <div className="text-indigo-700 group-hover:text-white mr-3 mb-2">
                        <SettingOutlined />
                      </div>
                      <p className="text-indigo-700 group-hover:text-white group-hover:font-bold text-sm">
                        Manage Game
                      </p>
                    </div>
                  </NavLink>
                  <NavLink
                    to="/games-list/create"
                    activeClassName="selected-sidebar"
                    className="group text-decoration-none navigasi-sidebar"
                  >
                    <div className="transition duration-150 group-hover:bg-indigo-500 px-3 py-2 mt-1 rounded flex justify-start items-center">
                      <div className="text-indigo-700 group-hover:text-white mr-3 mb-2">
                        <PlusCircleOutlined />
                      </div>
                      <p className="text-indigo-700 group-hover:text-white group-hover:font-bold text-sm">
                        Add Game
                      </p>
                    </div>
                  </NavLink>
                  <div className="mb-4 mt-3">
                    <p
                      className="font-weight-bold"
                      style={{ color: "lightgrey", fontSize: "9pt" }}
                    >
                      Account
                    </p>
                  </div>
                  <NavLink
                    to="/change-password"
                    activeClassName="selected-sidebar"
                    className="group text-decoration-none navigasi-sidebar"
                  >
                    <div className="transition duration-150 group-hover:bg-indigo-500 px-3 py-2 mt-1 rounded flex justify-start items-center">
                      <div className="text-indigo-700 group-hover:text-white mr-3 mb-2">
                        <KeyOutlined />
                      </div>
                      <p className="text-indigo-700 group-hover:text-white group-hover:font-bold text-sm">
                        Change Password
                      </p>
                    </div>
                  </NavLink>
                </div>
              </div>
            </div>
          </Sider>
        )}
    </>
  );
};

export default Sidebar;
