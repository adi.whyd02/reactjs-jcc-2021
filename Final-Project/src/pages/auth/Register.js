import { Form, Input, Button, message, Alert } from "antd";
import Image from "../../assets/img/login-flat.jpg";
import axios from "axios";
import Cookies from "js-cookie";
import { useHistory } from "react-router";
import { useState } from "react";
import { Link } from "react-router-dom";
import { ArrowLeftOutlined } from "@ant-design/icons";

const Register = () => {
  const history = useHistory();
  const [errorMessage, setErrorMessage] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const onFinish = (values) => {
    setIsLoading(true);

    axios
      .post(`https://backendexample.sanbersy.com/api/register `, values)
      .then((res) => {
        Cookies.set("name", res.data.user.name, { expires: 1 });
        Cookies.set("email", res.data.user.email, { expires: 1 });
        Cookies.set("token", res.data.token, { expires: 1 });
        setTimeout(() => {
          message.success(`Welcome ${res.data.user.name} !`);
        }, 1000);
        setIsLoading(false);

        history.push("/dashboard");
      })
      .catch((error) => {
        setIsLoading(false);

        const res = JSON.parse(error.response.data.replace(/\\/g, ""));
        setErrorMessage(res[Object.keys(res)[0]]);
      });
  };

  return (
    <div className="bg-white shadow-md rounded-xl w-1/2 absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2">
      <Link to="/">
        <div
          className="font-bold text-xl m-5 ml-10 mb-0 text-black"
          title="Back to home"
        >
          <ArrowLeftOutlined />
        </div>
      </Link>
      <div className="flex">
        <div>
          <img src={Image} alt="Flat illustration login" />
        </div>
        <div className="p-10">
          <div className="mb-10">
            <p className="text-2xl font-bold">Register</p>
            <p className="text-xs text-gray-500">
              To keep connected with us please register with your personal
              information by name, email address and password.
            </p>
          </div>
          {errorMessage.length > 0 && (
            <div className="mb-7">
              <Alert message={errorMessage} type="error" showIcon />
            </div>
          )}
          <Form
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="Name"
              name="name"
              rules={[{ required: true, message: "Please input your name!" }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Email"
              name="email"
              rules={[
                {
                  required: true,
                  message: "Please input your email!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="password"
              rules={[
                { required: true, message: "Please input your password!" },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item>
              <div className="flex justify-end">
                <Button
                  type="primary"
                  htmlType="submit"
                  style={{
                    background: "#6366F1",
                    color: "white",
                    borderRadius: "5px",
                    padding: "2px 20px",
                  }}
                >
                  {!isLoading ? "Submit" : "Loading..."}
                </Button>
              </div>
            </Form.Item>
          </Form>
          <p className="text-xs">
            Already have an account?
            <Link to="/login"> Login</Link>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Register;
