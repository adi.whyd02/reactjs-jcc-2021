import { Form, Input, Button, message, Alert } from "antd";
import Image from "../../assets/img/login-flat.jpg";
import axios from "axios";
import Cookies from "js-cookie";
import { useState } from "react";

const ChangePassword = () => {
  const [form] = Form.useForm();
  const [errorMessage, setErrorMessage] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const onFinish = (values) => {
    setIsLoading(true);

    axios
      .post(`https://backendexample.sanbersy.com/api/change-password`, values, {
        headers: {
          Authorization: "Bearer " + Cookies.get("token"),
        },
      })
      .then(() => {
        form.resetFields();
        setIsLoading(false);
        message.success(`Password changed successfully !`);
        setErrorMessage("");
      })
      .catch((error) => {
        setIsLoading(false);
        const res = error.response
          ? JSON.parse(error.response.data.replace(/\\/g, ""))
          : error.message;

        setErrorMessage(res[Object.keys(res)[0]]);
      });
  };

  return (
    <div className="bg-white shadow-md rounded-xl w-3/4 flex">
      <div>
        <img src={Image} alt="Flat illustration login" />
      </div>
      <div className="p-10">
        <div className="mb-10 mt-10">
          <p className="text-2xl font-bold">Change Password</p>
          <p className="text-xs text-gray-500">
            To change your password fill with current password and new password.
          </p>
        </div>
        {errorMessage.length > 0 && (
          <div className="mb-7">
            <Alert message={errorMessage} type="error" showIcon />
          </div>
        )}
        <Form
          name="basic"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          autoComplete="off"
          layout="vertical"
          form={form}
        >
          <Form.Item
            name="current_password"
            label="Current Password"
            rules={[
              {
                required: true,
                message: "Please input your current password!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="new_password"
            label="New Password"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="new_confirm_password"
            label="Confirm Password"
            dependencies={["password"]}
            rules={[
              {
                required: true,
                message: "Please confirm your password!",
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("new_password") === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    new Error(
                      "The two passwords that you entered do not match!"
                    )
                  );
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item>
            <div className="flex justify-end">
              <Button
                type="primary"
                htmlType="submit"
                style={{
                  background: "#6366F1",
                  color: "white",
                  borderRadius: "5px",
                  padding: "2px 20px",
                }}
              >
                {!isLoading ? "Submit" : "Loading..."}
              </Button>
            </div>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default ChangePassword;
