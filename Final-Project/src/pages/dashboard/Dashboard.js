import {
  VideoCameraOutlined,
  PictureOutlined,
  SettingOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import { Badge } from "antd";
import { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { GamesContext } from "../../context/games-context";
import { MoviesContext } from "../../context/movies-context";

const Dashboard = () => {
  const {
    dataMovies,
    isLoading: isLoadingMovies,
    api: apiMovies,
  } = useContext(MoviesContext);
  const { handleGetAll: handleGetAllMovies } = apiMovies;
  const [perGenreMovies, setPerGenreMovies] = useState([]);

  const {
    dataGames,
    isLoading: isLoadingGames,
    api: apiGames,
  } = useContext(GamesContext);
  const { handleGetAll: handleGetAllGames } = apiGames;
  const [perGenreGames, setPerGenreGames] = useState([]);

  useEffect(() => {
    handleGetAllMovies();
    handleGetAllGames();
  }, []);

  useEffect(() => {
    let genresMovies = [];
    dataMovies.map((item) => {
      item.genre.split(",").map((genre) => {
        const lowercased = genresMovies.map((genre) => genre.toLowerCase());
        lowercased.indexOf(genre.toLowerCase()) === -1 &&
          genresMovies.push(genre);
      });
    });

    const dividePerGenreMovies = genresMovies.map((genre) => {
      let data = [];
      dataMovies.filter((i) => {
        if (i.genre.toLowerCase().indexOf(genre.toLowerCase()) >= 0) {
          data.push(i);
        }
      });

      return {
        genre,
        data,
      };
    });
    setPerGenreMovies(dividePerGenreMovies);

    let genresGames = [];
    dataGames.map((item) => {
      item.genre.split(",").map((genre) => {
        const lowercased = genresGames.map((genre) => genre.toLowerCase());
        lowercased.indexOf(genre.toLowerCase()) === -1 &&
          genresGames.push(genre);
      });
    });

    const dividePerGenreGames = genresGames.map((genre) => {
      let data = [];
      dataGames.filter((i) => {
        if (i.genre.toLowerCase().indexOf(genre.toLowerCase()) >= 0) {
          data.push(i);
        }
      });

      return {
        genre,
        data,
      };
    });
    setPerGenreGames(dividePerGenreGames);
  }, [dataMovies, dataGames]);

  const colors = [
    "pink",
    "red",
    "yellow",
    "orange",
    "cyan",
    "green",
    "blue",
    "purple",
    "geekblue",
    "magenta",
    "volcano",
    "gold",
    "lime",
    "pink",
    "red",
    "yellow",
    "orange",
    "cyan",
    "green",
    "blue",
    "purple",
    "geekblue",
    "magenta",
    "volcano",
    "gold",
    "lime",
  ];
  return (
    <div>
      <div className="mb-15 ml-24">
        <h1 className="font-bold"></h1>
        <p className="text-lg font-bold mb-5 text-indigo-700">Dashboard</p>
      </div>
      <div className="flex justify-around flex-wrap items-start">
        <div className="w-1/3 mt-10">
          <p className="text-sm font-bold mb-5 text-black">Movies</p>
          <div className="flex flex-col items-stretch">
            <div className="flex space-x-5 w-full">
              <div className="w-1/2 bg-indigo-200 shadow-sm rounded-md p-3 pr-12">
                <Link to="/movies-list">
                  <div className="flex items-center">
                    <div className="flex-shrink-0 w-16 h-16 mr-4 bg-white rounded-full text-indigo-700 font-bold flex justify-center items-center text-xl">
                      <SettingOutlined />
                    </div>
                    <div>
                      <p className="font-bold mb-2 text-black">Manage</p>
                    </div>
                  </div>
                </Link>
              </div>
              <div className="w-1/2 bg-indigo-200 shadow-sm rounded-md p-3 pr-12">
                <Link to="/movies-list/create">
                  <div className="flex items-center">
                    <div className="flex-shrink-0 w-16 h-16 mr-4 bg-white rounded-full text-indigo-700 font-bold flex justify-center items-center text-xl">
                      <PlusOutlined />
                    </div>
                    <div>
                      <p className="font-bold mb-2 text-black">Add</p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
          <div className="mt-5">
            <div className="bg-white shadow-sm rounded-md p-7">
              <p className="text-sm font-bold mb-5 text-indigo-700">Details</p>
              <div className="bg-gray-50 p-3 rounded-md mb-8">
                <div className="flex items-center space-x-5">
                  <div className="flex-shrink-0 w-20 h-20 bg-white rounded-full text-indigo-700 font-bold flex justify-center items-center text-2xl">
                    <VideoCameraOutlined />
                  </div>
                  <div>
                    <p className="text-xs font-bold mb-1 text-black">
                      Total Movies
                    </p>
                    <p className="text-xl text-black mb-0">
                      {dataMovies.length}{" "}
                      <span className="text-xs text-black">Items</span>
                    </p>
                  </div>
                </div>
              </div>
              <p className="text-xs font-bold">In Genre</p>
              <div className="flex justify-start flex-wrap">
                {perGenreMovies.map((value, index) => (
                  <div key={value.genre} className="mr-3">
                    <Badge
                      color={colors[index]}
                      text={`${value.genre} (${value.data.length})`}
                    />
                  </div>
                ))}
                {perGenreMovies.length === 0 && (
                  <p className="text-xs">No genre list</p>
                )}
              </div>
            </div>
          </div>
        </div>

        <div className="w-1/3 mt-10">
          <p className="text-sm font-bold mb-5 text-black">Games</p>
          <div className="flex flex-col items-stretch">
            <div className="flex space-x-5 w-full">
              <div className="w-1/2 bg-indigo-200 shadow-sm rounded-md p-3 pr-12">
                <Link to="/games-list">
                  <div className="flex items-center">
                    <div className="flex-shrink-0 w-16 h-16 mr-4 bg-white rounded-full text-indigo-700 font-bold flex justify-center items-center text-xl">
                      <SettingOutlined />
                    </div>
                    <div>
                      <p className="font-bold mb-2 text-black">Manage</p>
                    </div>
                  </div>
                </Link>
              </div>
              <div className="w-1/2 bg-indigo-200 shadow-sm rounded-md p-3 pr-12">
                <Link to="/games-list/create">
                  <div className="flex items-center">
                    <div className="flex-shrink-0 w-16 h-16 mr-4 bg-white rounded-full text-indigo-700 font-bold flex justify-center items-center text-xl">
                      <PlusOutlined />
                    </div>
                    <div>
                      <p className="font-bold mb-2 text-black">Add</p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
          <div className="mt-5">
            <div className="bg-white shadow-sm rounded-md p-7">
              <p className="text-sm font-bold mb-5 text-indigo-700">Details</p>
              <div className="bg-gray-50 p-3 rounded-md mb-8">
                <div className="flex items-center space-x-5">
                  <div className="flex-shrink-0 w-20 h-20 bg-white rounded-full text-indigo-700 font-bold flex justify-center items-center text-2xl">
                    <PictureOutlined />
                  </div>
                  <div>
                    <p className="text-xs font-bold mb-1 text-black">
                      Total Games
                    </p>
                    <p className="text-xl text-black mb-0">
                      {dataGames.length}{" "}
                      <span className="text-xs text-black">Items</span>
                    </p>
                  </div>
                </div>
              </div>
              <p className="text-xs font-bold">In Genre</p>
              <div className="flex justify-start flex-wrap">
                {perGenreGames.map((value, index) => (
                  <div key={value.genre} className="mr-3">
                    <Badge
                      color={colors[index]}
                      text={`${value.genre} (${value.data.length})`}
                    />
                  </div>
                ))}
                {perGenreGames.length === 0 && (
                  <p className="text-xs">No genre list</p>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
