import { Rate } from "antd";
import {
  ClockCircleOutlined,
  AlignCenterOutlined,
  LoadingOutlined,
} from "@ant-design/icons";
import { useContext, useEffect, useState } from "react";
import { useParams } from "react-router";
import { MoviesContext } from "../../context/movies-context";

const MoviesDetail = () => {
  const { api, isLoading } = useContext(MoviesContext);
  const { handleRead } = api;

  const [item, setItem] = useState({});
  const { id } = useParams();

  const load = async () => {
    const data = await handleRead(id);
    setItem(data);
  };

  useEffect(() => {
    load();
  }, [id]);
  return (
    <div>
      <div className="w-full p-4">
        <div className="c-card block bg-white shadow-md hover:shadow-xl rounded-lg overflow-hidden">
          {!isLoading && (
            <div className="flex">
              <div className="flex-shrink-0">
                <img
                  className="inset-0 object-cover w-96 h-full"
                  src={item.image_url}
                  alt={item.image_url}
                />
              </div>
              <div className="p-6">
                <div className="p-4">
                  <h1 className="mt-2 mb-2 text-xl font-bold">{item.title}</h1>
                  <p className="text-xs font-bold text-gray-500">
                    {item.genre}
                  </p>
                </div>
                <div className="p-4 w-3/4">
                  <p className="text-xs text-gray-500">{item.description}</p>
                </div>
                <div className="p-4 border-t border-b text-xs text-gray-700 w-3/4 flex flex-wrap justify-between">
                  <span className="flex items-center text-gray-900 my-2">
                    <AlignCenterOutlined className="mr-2" />
                    {item.year}
                  </span>
                  <span className="flex items-center mb-1 text-gray-900 my-2">
                    <ClockCircleOutlined className="mr-2" /> {item.duration}{" "}
                    minutes
                  </span>
                </div>
                <div className="p-4 flex items-center text-sm text-gray-600">
                  <Rate
                    disabled
                    value={item.rating}
                    count={10}
                    style={{ fontSize: "11pt" }}
                  />
                  <span className="ml-2 text-xs">{item.rating}/10</span>
                </div>
                <div className="p-4 w-3/4">
                  <p className="text-xs font-bold text-gray-700">Review</p>
                  <p className="text-xs text-gray-500">{item.review}</p>
                </div>
              </div>
            </div>
          )}

          {isLoading && (
            <div className="w-full h-96 flex justify-center items-center text-5xl text-indigo-700">
              <LoadingOutlined />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default MoviesDetail;
