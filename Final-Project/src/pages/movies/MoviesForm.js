import { Button, Form, Input, InputNumber } from "antd";
import { useContext, useEffect } from "react";
import { useHistory, useParams } from "react-router";
import { Link } from "react-router-dom";
import { MoviesContext } from "../../context/movies-context";

const MoviesForm = () => {
  const [form] = Form.useForm();
  const { currentId, setCurrentId, isLoading, api } = useContext(MoviesContext);
  const { handleRead, handleCreate, handleUpdate } = api;
  const history = useHistory();
  const { id } = useParams();

  const load = async () => {
    const data = await handleRead(id);
    setCurrentId(id);
    form.setFieldsValue(data);
  };

  useEffect(() => {
    if (id) {
      load();
    } else {
      form.setFieldsValue({ release_year: 2007 });
    }

    return () => {
      setCurrentId(null);
    };
  }, [id]);

  const handleSubmit = async (values) => {
    if (currentId === null) {
      await handleCreate(values);
    } else {
      await handleUpdate(values);
    }
    history.push("/movies-list");
  };

  return (
    <div className="bg-white shadow-md rounded-xl p-10">
      <h1 className="text-lg font-bold mb-10">Movie Form</h1>
      <Form layout="vertical" form={form} onFinish={handleSubmit}>
        <Form.Item
          name="title"
          label="Title"
          className="w-full"
          rules={[
            {
              required: true,
              message: "Please input title!",
              whitespace: true,
            },
          ]}
        >
          <Input style={{ width: "100%" }} />
        </Form.Item>
        <Form.Item
          name="description"
          label="Description"
          rules={[
            {
              required: true,
              message: "Please input description!",
              whitespace: true,
            },
          ]}
        >
          <Input.TextArea />
        </Form.Item>
        <div className="flex justify-between space-x-5">
          <Form.Item
            name="genre"
            label="Genre"
            className="w-1/2"
            rules={[
              {
                required: true,
                message: "Please input genre!",
                whitespace: true,
              },
            ]}
          >
            <Input
              min="1980"
              max="2021"
              style={{ width: "100%" }}
              placeholder="Action, Crime, Drama"
            />
          </Form.Item>
          <Form.Item
            name="image_url"
            label="Image Url"
            className="w-1/2"
            rules={[
              {
                required: true,
                message: "Please input image url!",
                whitespace: true,
              },
            ]}
          >
            <Input style={{ width: "100%" }} />
          </Form.Item>
        </div>
        <div className="flex justify-between space-x-5">
          <Form.Item
            name="year"
            label="Year"
            className="w-1/3"
            rules={[
              {
                required: true,
                message: "Please input year!",
              },
              () => ({
                validator(_, value) {
                  if (value >= 1980 && value <= 2021) {
                    return Promise.resolve();
                  }

                  return Promise.reject(
                    new Error("Must be between 1980-2021!")
                  );
                },
              }),
            ]}
          >
            <InputNumber style={{ width: "100%" }} />
          </Form.Item>
          <Form.Item
            name="duration"
            label="Duration (Minute)"
            className="w-1/3"
            rules={[
              {
                required: true,
                message: "Please input duration!",
              },
            ]}
          >
            <InputNumber style={{ width: "100%" }} />
          </Form.Item>
          <Form.Item
            name="rating"
            label="Rating"
            className="w-1/3"
            rules={[
              { required: true, message: "Please input rating!" },
              () => ({
                validator(_, value) {
                  if (value >= 0 && value <= 10) {
                    return Promise.resolve();
                  }

                  return Promise.reject(new Error("Must be between 0-10!"));
                },
              }),
            ]}
          >
            <InputNumber style={{ width: "100%" }} />
          </Form.Item>
        </div>
        <Form.Item
          name="review"
          label="Review"
          rules={[
            {
              required: true,
              message: "Please input review!",
              whitespace: true,
            },
          ]}
        >
          <Input.TextArea />
        </Form.Item>
        <div className="w-full flex justify-end items-center mt-10">
          <Form.Item>
            <Button
              key="submit"
              style={{
                background: "#6366F1",
                color: "white",
                padding: "5px 75px",
                borderRadius: "10px",
              }}
              htmlType="submit"
              size="large"
            >
              {!isLoading ? "Submit" : "Loading..."}
            </Button>
          </Form.Item>
        </div>
      </Form>
      <Link to="/movies-list">Back to table</Link>
    </div>
  );
};

export default MoviesForm;
