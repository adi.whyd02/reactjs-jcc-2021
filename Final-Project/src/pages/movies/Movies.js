import { MoviesContext } from "../../context/movies-context";
import { useContext, useEffect, useState } from "react";
import MoviesCard from "../../components/movies/MoviesCard";
import {
  FilterOutlined,
  SearchOutlined,
  DownOutlined,
} from "@ant-design/icons";
import { Badge, Dropdown, Menu, Form } from "antd";
import MoviesModalFilter from "../../components/movies/MoviesModalFilter";

const Movies = () => {
  const { dataMovies, isLoading, api } = useContext(MoviesContext);
  const { handleGetAll } = api;

  const [data, setData] = useState([]);

  const [visibleModal, setVisibleModal] = useState(false);
  const [form] = Form.useForm();
  const [isFiltered, setIsFiltered] = useState(false);

  const [search, setSearch] = useState("");

  const [sort, setSort] = useState("Recommendation");

  useEffect(() => {
    handleGetAll();
  }, []);

  useEffect(() => {
    setData(dataMovies);
  }, [dataMovies]);

  const handleSearch = (e) => {
    handleReset();
    setSearch(e.target.value);

    if (!e.target.value) {
      setData(dataMovies);
    } else {
      const filter = dataMovies.filter((item) => {
        return (
          item.title.toLowerCase().indexOf(e.target.value.toLowerCase()) >= 0
        );
      });
      setData(filter);
    }
  };

  const handleSort = (key) => {
    handleReset();
    setSearch("");
    if (key === "recommendation") {
      setSort("Recommendation");
      const filterRecommendationMovies = dataMovies.sort((a, b) => {
        return parseFloat(a.id) - parseFloat(b.id);
      });
      setData(filterRecommendationMovies);
    } else if (key === "highest_rated") {
      setSort("Highest Rated");
      const filterHighestRatedMovies = dataMovies.sort((a, b) => {
        return parseFloat(b.rating) - parseFloat(a.rating);
      });
      setData(filterHighestRatedMovies);
    } else if (key === "most_recent") {
      setSort("Most Recent");
      const filterMostRecentMovies = dataMovies.sort((a, b) => {
        return parseFloat(b.year) - parseFloat(a.year);
      });
      setData(filterMostRecentMovies);
    }
  };

  const handleFilter = (values) => {
    setIsFiltered(true);
    setSearch("");
    if (!values.rating && !values.year && !values.duration) {
      setData(dataMovies);
    } else {
      const filter = dataMovies.filter((item) => {
        return (
          (values.rating ? item.rating === parseInt(values.rating) : true) &&
          (values.year ? item.year === parseInt(values.year) : true) &&
          (values.duration ? item.duration <= parseInt(values.duration) : true)
        );
      });
      setData(filter);
    }
    setVisibleModal(false);
  };

  const handleReset = () => {
    setIsFiltered(false);
    form.resetFields();
    setData(dataMovies);
  };

  const filterFeature = {
    visibleModal,
    setVisibleModal,
    form,
    handleFilter,
    handleReset,
    isFiltered,
  };

  const ddprofil = (
    <Menu>
      <Menu.Item
        key="recommendation"
        onClick={() => handleSort("recommendation")}
      >
        <span className="text-xs">Recommendation</span>
      </Menu.Item>
      <Menu.Item
        key="highest_rated"
        onClick={() => handleSort("highest_rated")}
      >
        <span className="text-xs">Highest Rated</span>
      </Menu.Item>
      <Menu.Item key="most_recent" onClick={() => handleSort("most_recent")}>
        <span className="text-xs">Most Recent</span>
      </Menu.Item>
    </Menu>
  );

  return (
    <div>
      <div className="flex justify-between items-center mb-16">
        <div>
          <p className="text-lg font-bold text-indigo-700 ml-5">Movies</p>
        </div>
        <div className="flex space-x-10">
          <div className="w-80 bg-gray-200 p-2 rounded-md flex items-center space-x-2 px-5">
            <SearchOutlined style={{ color: "#4338CA" }} />
            <input
              type="text"
              placeholder="Search by title"
              className="w-60 px-2 text-indigo-700 outline-none bg-transparent"
              value={search}
              onChange={handleSearch}
            />
          </div>
          <div>
            <Badge dot={isFiltered}>
              <div
                onClick={() => setVisibleModal(true)}
                title="Filter"
                className="bg-gray-200 p-3 rounded-md flex items-center font-bold text-indigo-700 cursor-pointer hover:bg-indigo-500 hover:text-white"
              >
                <FilterOutlined />
              </div>
            </Badge>
            <MoviesModalFilter filterFeature={filterFeature} />
          </div>
        </div>
      </div>
      <div className="mb-7 ml-7">
        <Dropdown overlay={ddprofil}>
          <div className="bg-indigo-100 text-indigo-500 w-48 p-2 px-5 rounded-lg flex justify-between items-center space-x-5 cursor-pointer">
            <p className="mb-0">{sort}</p>
            <DownOutlined />
          </div>
        </Dropdown>
      </div>
      <MoviesCard data={data} isLoading={isLoading} />
      {!isLoading && data.length === 0 && <p className="ml-5">No data.</p>}
    </div>
  );
};

export default Movies;
