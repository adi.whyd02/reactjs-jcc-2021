import { MoviesContext } from "../../context/movies-context";
import { useContext, useEffect, useState } from "react";
import MoviesCard from "../../components/movies/MoviesCard";
import GamesCard from "../../components/games/GamesCard";
import { GamesContext } from "../../context/games-context";
import { Link } from "react-router-dom";
import CardSkeleton from "../../components/skeloton/CardSkeleton";

const Home = () => {
  const {
    dataMovies,
    isLoading: isLoadingMovies,
    api: apiMovies,
  } = useContext(MoviesContext);
  const { handleGetAll: handleGetAllMovies } = apiMovies;

  const {
    dataGames,
    isLoading: isLoadingGames,
    api: apiGames,
  } = useContext(GamesContext);
  const { handleGetAll: handleGetAllGames } = apiGames;

  const [highestRatedMovies, setHighestRatedMovies] = useState([]);
  const [mostRecentMovies, setMostRecentMovies] = useState([]);
  const [mostRecentGames, setMostRecentGames] = useState([]);

  useEffect(() => {
    handleGetAllMovies();
    handleGetAllGames();
  }, []);
  useEffect(() => {
    const filterHighestRatedMovies = dataMovies.sort((a, b) => {
      return parseFloat(b.rating) - parseFloat(a.rating);
    });
    setHighestRatedMovies(filterHighestRatedMovies.slice(0, 4));

    const filterMostRecentMovies = dataMovies.sort((a, b) => {
      return parseFloat(b.year) - parseFloat(a.year);
    });
    setMostRecentMovies(filterMostRecentMovies.slice(0, 4));

    const filterMostRecentGames = dataGames.sort((a, b) => {
      return parseFloat(b.release) - parseFloat(a.release);
    });
    setMostRecentGames(filterMostRecentGames.slice(0, 4));
  }, [dataMovies, dataGames]);

  return (
    <div>
      <div className="mb-20 border-b-2 pb-20">
        <div className="flex justify-between items-center">
          <p className="text-lg font-bold mb-5 text-indigo-700 ml-5">
            Highest Rated Movies
          </p>
          <Link to="/movies">
            <button className="bg-indigo-200 text-indigo-700 font-bold hover:bg-indigo-500 hover:text-white px-7 py-2 rounded-md mr-5">
              View All
            </button>
          </Link>
        </div>
        {!isLoadingMovies && <MoviesCard data={highestRatedMovies} />}
        {!isLoadingMovies && highestRatedMovies.length === 0 && (
          <p className="ml-5">No data.</p>
        )}
        <div>{isLoadingMovies && <CardSkeleton />}</div>
      </div>
      <div className="mb-20 border-b-2 pb-20">
        <div className="flex justify-between items-center">
          <p className="text-lg font-bold mb-5 text-indigo-700 ml-5">
            Most Recent Movies
          </p>
          <Link to="/movies">
            <button className="bg-indigo-200 text-indigo-700 font-bold hover:bg-indigo-500 hover:text-white px-7 py-2 rounded-md mr-5">
              View All
            </button>
          </Link>
        </div>
        {!isLoadingMovies && <MoviesCard data={mostRecentMovies} />}
        {!isLoadingMovies && mostRecentMovies.length === 0 && (
          <p className="ml-5">No data.</p>
        )}
        <div>{isLoadingMovies && <CardSkeleton />}</div>
      </div>
      <div className="mb-20 border-b-2 pb-20">
        <div className="flex justify-between items-center">
          <p className="text-lg font-bold mb-5 text-indigo-700 ml-5">
            Most Recent Games
          </p>
          <Link to="/games">
            <button className="bg-indigo-200 text-indigo-700 font-bold hover:bg-indigo-500 hover:text-white px-7 py-2 rounded-md mr-5">
              View All
            </button>
          </Link>
        </div>
        {!isLoadingGames && <GamesCard data={mostRecentGames} />}
        {!isLoadingGames && mostRecentGames.length === 0 && (
          <p className="ml-5">No data.</p>
        )}
        <div>{isLoadingGames && <CardSkeleton />}</div>
      </div>
    </div>
  );
};

export default Home;
