import React, { useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { GamesContext } from "../../context/games-context";
import { Table, Button, Form, Badge } from "antd";
import {
  DeleteOutlined,
  EditOutlined,
  PictureOutlined,
} from "@ant-design/icons";
import TableListHeader from "../../components/table-list/TableHeader";

const GamesList = () => {
  const { dataGames, isLoading, api } = useContext(GamesContext);
  const { handleGetAll, handleDelete } = api;

  const [data, setData] = useState([]);

  const [sortedInfo, setSortedInfo] = useState({});
  const [search, setSearch] = useState("");
  const [tabGenre, setTabGenre] = useState("");
  const [tabList, setTabList] = useState([]);

  const [visibleModal, setVisibleModal] = useState(false);
  const [form] = Form.useForm();
  const [isFiltered, setIsFiltered] = useState(false);

  const [perGenreGames, setPerGenreGames] = useState([]);
  const colors = [
    "pink",
    "red",
    "yellow",
    "orange",
    "cyan",
    "green",
    "blue",
    "purple",
    "geekblue",
    "magenta",
    "volcano",
    "gold",
    "lime",
    "pink",
    "red",
    "yellow",
    "orange",
    "cyan",
    "green",
    "blue",
    "purple",
    "geekblue",
    "magenta",
    "volcano",
    "gold",
    "lime",
  ];

  const deleteItem = async (id) => {
    await handleDelete(id);
  };
  useEffect(() => {
    handleGetAll();
  }, []);
  useEffect(() => {
    setData(dataGames);
    let genresGames = [];
    dataGames.map((item) => {
      item.genre.split(",").map((genre) => {
        const lowercased = genresGames.map((genre) => genre.toLowerCase());
        lowercased.indexOf(genre.toLowerCase()) === -1 &&
          genresGames.push(genre);
      });
    });
    setTabList(["All", ...genresGames]);

    const dividePerGenreGames = genresGames.map((genre) => {
      let data = [];
      dataGames.filter((i) => {
        if (i.genre.toLowerCase().indexOf(genre.toLowerCase()) >= 0) {
          data.push(i);
        }
      });

      return {
        genre,
        data,
      };
    });
    setPerGenreGames(dividePerGenreGames);
  }, [dataGames]);

  const handleChangeTable = (pagination, filters, sorter) => {
    setSortedInfo(sorter);
  };
  const sortingTable = (field) => {
    return sortedInfo.columnKey === field && sortedInfo.order;
  };

  const columns = [
    {
      title: "No",
      dataIndex: "no",
      key: "no",
      sorter: (a, b) => a.no - b.no,
      sortOrder: sortingTable("no"),
      width: 60,
    },
    {
      title: "Image",
      key: "image_url",
      render: (text, record) => (
        <img
          src={record.image_url}
          alt={record.image_url}
          width={90}
          className="rounded-lg h-32"
        />
      ),
      width: 130,
      sorter: (a, b) => a.image_url.localeCompare(b.image_url),
      sortOrder: sortingTable("image_url"),
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      sorter: (a, b) => a.name.localeCompare(b.name),
      sortOrder: sortingTable("name"),
    },
    {
      title: "Genre",
      dataIndex: "genre",
      key: "genre",
      sorter: (a, b) => a.genre.localeCompare(b.genre),
      sortOrder: sortingTable("genre"),
      responsive: ["lg"],
    },
    {
      title: "Release",
      dataIndex: "release",
      key: "release",
      sorter: (a, b) => a.release - b.release,
      sortOrder: sortingTable("release"),
      responsive: ["lg"],
    },
    {
      title: "Platform",
      dataIndex: "platform",
      key: "platform",
      sorter: (a, b) => a.platform.localeCompare(b.platform),
      sortOrder: sortingTable("platform"),
      ellipsis: true,
      responsive: ["lg"],
    },
    {
      title: "Single Player",
      dataIndex: "isSinglePlayer",
      key: "isSinglePlayer",
      sorter: (a, b) => a.isSinglePlayer.localeCompare(b.isSinglePlayer),
      sortOrder: sortingTable("isSinglePlayer"),
      ellipsis: true,
      responsive: ["lg"],
    },
    {
      title: "Multi Player",
      dataIndex: "isMultiPlayer",
      key: "isMultiPlayer",
      sorter: (a, b) => a.isMultiPlayer.localeCompare(b.isMultiPlayer),
      sortOrder: sortingTable("isMultiPlayer"),
      ellipsis: true,
      responsive: ["lg"],
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <>
          <Link
            to={`/games-list/edit/${record.id}`}
            style={{ marginRight: "10px" }}
          >
            <Button
              style={{
                background: "#C7D2FE",
                padding: "5px 15px 30px 15px",
                marginRight: "7px",
                color: "#6366F1",
                fontWeight: "bold",
                borderRadius: "10px",
              }}
              title="Edit"
            >
              <EditOutlined />
            </Button>
          </Link>
          <Button
            style={{
              background: "#C7D2FE",
              padding: "5px 15px 30px 15px",
              marginRight: "7px",
              color: "#6366F1",
              fontWeight: "bold",
              borderRadius: "10px",
            }}
            title="Delete"
            onClick={() => deleteItem(record.id)}
          >
            <DeleteOutlined />
          </Button>
        </>
      ),
    },
  ];

  const handleChangeTab = (key) => {
    setTabGenre(key.toLowerCase());

    setSearch("");
    setIsFiltered(false);
    form.resetFields();

    if (key.toLowerCase() === "all") {
      setData(dataGames);
    } else {
      const filter = dataGames.filter((item) => {
        return item.genre.toLowerCase().indexOf(key.toLowerCase()) >= 0;
      });
      setData(filter);
    }
  };
  const handleSearch = (e) => {
    handleReset();
    setSearch(e.target.value);

    if (!e.target.value) {
      handleChangeTab(tabGenre);
    } else {
      const filter = dataGames.filter((item) => {
        return (
          item.name.toLowerCase().indexOf(e.target.value.toLowerCase()) >= 0 &&
          item.genre
            .toLowerCase()
            .indexOf(tabGenre === "all" ? "" : tabGenre) >= 0
        );
      });
      setData(filter);
    }
  };

  const handleFilter = (values) => {
    setIsFiltered(true);
    setSearch("");

    if (!values.release && !values.platform && !values.player) {
      handleChangeTab(tabGenre);
    } else {
      const cekDataTab = dataGames.filter((item) => {
        return (
          item.genre
            .toLowerCase()
            .indexOf(tabGenre === "all" ? "" : tabGenre) >= 0
        );
      });

      const filter = cekDataTab.filter((item) => {
        return (
          (values.platform
            ? item.platform
                .toLowerCase()
                .indexOf(values.platform.toLowerCase()) >= 0
            : true) &&
          (values.release
            ? parseInt(item.release) === parseInt(values.release)
            : true) &&
          (values.player.length > 0
            ? item.singlePlayer ===
                (values.player.indexOf("singlePlayer") >= 0 ? 1 : -1) ||
              item.multiplayer ===
                (values.player.indexOf("multiPlayer") >= 0 ? 1 : -1)
            : true)
        );
      });

      setData(filter);
    }
    setVisibleModal(false);
  };

  const handleReset = () => {
    setIsFiltered(false);
    form.resetFields();
    handleChangeTab(tabGenre);
  };

  const filterFeature = {
    visibleModal,
    setVisibleModal,
    form,
    handleFilter,
    handleReset,
    isFiltered,
  };

  const tabFeature = {
    tabList,
    handleChangeTab,
  };

  const searchFeature = {
    search,
    handleSearch,
  };

  return (
    <>
      <p className="text-lg font-bold mb-5 text-indigo-700">Games</p>

      <div className="flex items-start flex-wrap mb-10">
        <div className="bg-white shadow-sm rounded-md w-96 p-7 mr-5">
          <div className="flex items-center space-x-5">
            <div className="w-20 h-20 bg-gray-50 rounded-full text-indigo-700 font-bold flex justify-center items-center text-2xl">
              <PictureOutlined />
            </div>
            <div>
              <p className="text-2xl text-indigo-700 mb-0">
                {dataGames.length}{" "}
                <span className="text-xs text-indigo-700">Total Items</span>
              </p>
            </div>
          </div>
        </div>
        <div
          className="bg-white shadow-sm rounded-md p-7 mt-0"
          style={{ minWidth: 300, minHeight: 137 }}
        >
          <p className="text-xs font-bold mb-5">In Genre</p>
          <div className="flex justify-start flex-wrap">
            {perGenreGames.map((value, index) => (
              <div key={value.genre} className="mr-3">
                <Badge
                  color={colors[index]}
                  text={`${value.genre} (${value.data.length})`}
                />
              </div>
            ))}
            {perGenreGames.length === 0 && (
              <p className="text-xs">No genre list</p>
            )}
          </div>
        </div>
      </div>
      <div className="bg-white shadow-md rounded-xl p-10">
        <TableListHeader
          name="games"
          filterFeature={filterFeature}
          tabFeature={tabFeature}
          searchFeature={searchFeature}
        />
        <Table
          columns={columns}
          dataSource={data}
          rowKey="id"
          onChange={handleChangeTable}
          size="middle"
          loading={isLoading}
          pagination={{ pageSize: 5 }}
        />
      </div>
    </>
  );
};

export default GamesList;
