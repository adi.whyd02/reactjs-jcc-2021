import { Button, Form, Input, InputNumber, Checkbox } from "antd";
import { useContext, useEffect, useState } from "react";
import { useHistory, useParams } from "react-router";
import { Link } from "react-router-dom";
import { GamesContext } from "../../context/games-context";

const GamesForm = () => {
  const [form] = Form.useForm();
  const { currentId, setCurrentId, isLoading, api } = useContext(GamesContext);
  const { handleRead, handleCreate, handleUpdate } = api;
  const history = useHistory();
  const { id } = useParams();

  const load = async () => {
    const data = await handleRead(id);
    setCurrentId(id);
    form.setFieldsValue({
      ...data,
      player: [
        data.singlePlayer ? "singlePlayer" : null,
        data.multiplayer ? "multiPlayer" : null,
      ],
    });
  };

  useEffect(() => {
    if (id) {
      load();
    } else {
      form.setFieldsValue({ release_year: 2007 });
    }

    return () => {
      setCurrentId(null);
    };
  }, [id]);

  const handleSubmit = async (values) => {
    if (currentId === null) {
      await handleCreate(values);
    } else {
      await handleUpdate(values);
    }
    history.push("/games-list");
  };

  return (
    <div className="bg-white shadow-md rounded-xl p-10">
      <h1 className="text-lg font-bold mb-10">Game Form</h1>
      <Form layout="vertical" form={form} onFinish={handleSubmit}>
        <Form.Item
          name="name"
          label="Name"
          className="w-full"
          rules={[
            {
              required: true,
              message: "Please input name!",
              whitespace: true,
            },
          ]}
        >
          <Input style={{ width: "100%" }} />
        </Form.Item>
        <div className="flex justify-between space-x-5">
          <Form.Item
            name="genre"
            label="Genre"
            className="w-1/2"
            rules={[
              {
                required: true,
                message: "Please input genre!",
                whitespace: true,
              },
            ]}
          >
            <Input
              min="1980"
              max="2021"
              style={{ width: "100%" }}
              placeholder="Action, Adventure"
            />
          </Form.Item>
          <Form.Item
            name="image_url"
            label="Image Url"
            className="w-1/2"
            rules={[
              {
                required: true,
                message: "Please input image url!",
                whitespace: true,
              },
            ]}
          >
            <Input style={{ width: "100%" }} />
          </Form.Item>
        </div>
        <div className="flex justify-between space-x-5">
          <Form.Item
            name="release"
            label="Release"
            className="w-1/3"
            rules={[
              {
                required: true,
                message: "Please input release!",
              },
              () => ({
                validator(_, value) {
                  if (value >= 2000 && value <= 2021) {
                    return Promise.resolve();
                  }

                  return Promise.reject(
                    new Error("Must be between 2000 -2021!")
                  );
                },
              }),
            ]}
          >
            <InputNumber style={{ width: "100%" }} />
          </Form.Item>
          <Form.Item
            name="platform"
            label="Platform"
            className="w-1/3"
            rules={[
              {
                required: true,
                message: "Please input platform!",
              },
            ]}
          >
            <Input style={{ width: "100%" }} />
          </Form.Item>
          <Form.Item
            name="player"
            label="Player"
            className="w-1/3"
            rules={[
              {
                required: true,
                message: "Please input player!",
              },
            ]}
          >
            <Checkbox.Group>
              <div className="flex space-x-5">
                <Checkbox value="singlePlayer">Single Player</Checkbox>
                <Checkbox value="multiPlayer">Multi Player</Checkbox>
              </div>
            </Checkbox.Group>
          </Form.Item>
        </div>
        <div className="w-full flex justify-end items-center mt-16">
          <Form.Item>
            <Button
              key="submit"
              style={{
                background: "#6366F1",
                color: "white",
                padding: "5px 75px",
                borderRadius: "10px",
              }}
              htmlType="submit"
              size="large"
            >
              {!isLoading ? "Submit" : "Loading..."}
            </Button>
          </Form.Item>
        </div>
      </Form>
      <Link to="/games-list">Back to table</Link>
    </div>
  );
};

export default GamesForm;
