import {
  NodeCollapseOutlined,
  AlignCenterOutlined,
  CheckCircleOutlined,
  LoadingOutlined,
} from "@ant-design/icons";
import { useContext, useEffect, useState } from "react";
import { useParams } from "react-router";
import { GamesContext } from "../../context/games-context";

const GamesDetail = () => {
  const { api, isLoading } = useContext(GamesContext);
  const { handleRead } = api;

  const [item, setItem] = useState({});
  const { id } = useParams();

  const load = async () => {
    const data = await handleRead(id);
    setItem(data);
  };

  useEffect(() => {
    load();
  }, [id]);
  return (
    <div>
      <div className="w-full p-4">
        <div className="c-card block bg-white shadow-md hover:shadow-xl rounded-lg overflow-hidden">
          {!isLoading && (
            <div className="flex">
              <div className="flex-shrink-0">
                <img
                  className="inset-0 object-cover w-96 h-full"
                  src={item.image_url}
                  alt={item.image_url}
                />
              </div>
              <div className="p-6">
                <div className="p-4">
                  <h1 className="mt-2 mb-2 text-xl font-bold">{item.name}</h1>
                  <p className="text-xs font-bold text-gray-500">
                    {item.genre}
                  </p>
                </div>
                <div className="p-4 border-t border-b text-xs text-gray-700 w-3/4 flex flex-wrap justify-between">
                  <span className="flex items-center text-gray-900 my-2">
                    <AlignCenterOutlined className="mr-2" />
                    {item.release}
                  </span>
                  <span className="flex items-center mb-1 text-gray-900 my-2">
                    <NodeCollapseOutlined className="mr-2" /> {item.platform}
                  </span>
                </div>
                <div className="p-4 flex space-x-5 items-center text-sm">
                  {item.singlePlayer === 1 && (
                    <div className="flex justify-start space-x-2 items-center">
                      <span className="text-blue-500 text-xl">
                        <CheckCircleOutlined />
                      </span>
                      <span className="ml-2 text-xs text-black mt-2">
                        Single Player
                      </span>
                    </div>
                  )}

                  {item.multiplayer === 1 && (
                    <div className="flex justify-start space-x-2 items-center">
                      <span className="text-blue-500 text-xl">
                        <CheckCircleOutlined />
                      </span>
                      <span className="ml-2 text-xs text-black mt-2">
                        Multi Player
                      </span>
                    </div>
                  )}
                  {(item.multiplayer === 0 && item.singlePlayer === 0) ||
                    (!item.multiplayer && !item.singlePlayer && (
                      <span className="ml-2 text-xs text-black mt-2">
                        The number of players does not exist
                      </span>
                    ))}
                </div>
              </div>
            </div>
          )}
          {isLoading && (
            <div className="w-full h-96 flex justify-center items-center text-5xl text-indigo-700">
              <LoadingOutlined />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default GamesDetail;
