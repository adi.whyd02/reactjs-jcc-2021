import React, { useState, useEffect, useContext } from "react";
import { MobileApsContext } from "../contexts/mobile-aps-context";
import { Link, useHistory, useParams } from "react-router-dom";
import {
  Form,
  Input,
  InputNumber,
  Checkbox,
  Button,
  DatePicker,
  Space,
} from "antd";

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 4 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 24,
      offset: 17,
    },
  },
};

const FormList = () => {
  const { currentId, setCurrentId, api } = useContext(MobileApsContext);
  const [isLoading, setIsLoading] = useState(false);
  const { handleRead, handleCreate, handleUpdate } = api;
  const history = useHistory();
  const { id } = useParams();

  const [form] = Form.useForm();

  const load = async () => {
    const data = await handleRead(id);
    setCurrentId(id);
    form.setFieldsValue({
      ...data,
      platform: [
        data.is_android_app ? "Android" : null,
        data.is_ios_app ? "IOS" : null,
      ],
    });
  };

  useEffect(() => {
    if (id) {
      setIsLoading(true);
      load();
      setIsLoading(false);
    } else {
      form.setFieldsValue({ release_year: 2007 });
    }

    return () => {
      setCurrentId(null);
    };
  }, [id]);

  const handleSubmit = async (values) => {
    setIsLoading(true);
    if (currentId === null) {
      await handleCreate(values);
    } else {
      await handleUpdate(values);
    }
    history.push("/mobile-list");
  };

  return (
    <>
      <h1 style={{ textAlign: "center", marginBottom: "20px" }}>
        Mobile Apps Form
      </h1>
      <Form
        {...formItemLayout}
        form={form}
        name="register"
        onFinish={handleSubmit}
        scrollToFirstError
        style={{ width: "100%" }}
      >
        <Form.Item
          name="name"
          label="Name"
          rules={[
            {
              required: true,
              message: "Please input name!",
              whitespace: true,
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="category"
          label="Category"
          rules={[
            {
              required: true,
              message: "Please input category!",
              whitespace: true,
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="description"
          label="Description"
          rules={[
            {
              required: true,
              message: "Please input description!",
              whitespace: true,
            },
          ]}
        >
          <Input.TextArea />
        </Form.Item>

        <Form.Item
          name="release_year"
          label="Year"
          rules={[
            {
              required: true,
              message: "Please input year release!",
            },
            () => ({
              validator(_, value) {
                if (value >= 2007 && value <= 2021) {
                  return Promise.resolve();
                }

                return Promise.reject(
                  new Error("Input year must be between 2007-2021!")
                );
              },
            }),
          ]}
        >
          <InputNumber style={{ width: "100%" }} />
        </Form.Item>

        <Form.Item
          name="size"
          label="Size"
          rules={[{ required: true, message: "Please input size!" }]}
        >
          <InputNumber style={{ width: "100%" }} />
        </Form.Item>

        <Form.Item
          name="price"
          label="Price"
          rules={[{ required: true, message: "Please input price!" }]}
        >
          <InputNumber style={{ width: "100%" }} />
        </Form.Item>

        <Form.Item
          name="rating"
          label="Rating"
          rules={[
            { required: true, message: "Please input rating!" },
            () => ({
              validator(_, value) {
                if (value >= 0 && value <= 5) {
                  return Promise.resolve();
                }

                return Promise.reject(new Error("Rating must be between 0-5!"));
              },
            }),
          ]}
        >
          <InputNumber style={{ width: "100%" }} />
        </Form.Item>

        <Form.Item
          name="image_url"
          label="Image Url"
          rules={[
            {
              required: true,
              message: "Please input image!",
              whitespace: true,
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="platform"
          label="Platform"
          rules={[
            {
              required: true,
              message: "Please input platform!",
            },
          ]}
        >
          <Checkbox.Group>
            <Checkbox value="Android" style={{ marginRight: "20px" }}>
              <b> Android </b>
            </Checkbox>
            <Checkbox value="IOS" style={{ marginRight: "20px" }}>
              <b> Ios </b>
            </Checkbox>
          </Checkbox.Group>
        </Form.Item>

        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" style={{ width: "150px" }}>
            {isLoading ? "Loading.." : "Submit"}
          </Button>
        </Form.Item>
      </Form>

      <Link to="/mobile-list">Back to table</Link>
    </>
  );
};

export default FormList;
