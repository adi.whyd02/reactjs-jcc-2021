import { useEffect, useState, useContext } from "react";
import { useParams } from "react-router-dom";
import { MobileApsContext } from "../contexts/mobile-aps-context";
import ItemList from "./ItemList";

const Search = () => {
  const { data, api } = useContext(MobileApsContext);
  const { handleGetAll } = api;

  const { valueOfSearch } = useParams();

  useEffect(() => {
    handleGetAll();
  }, []);

  return (
    <>
      <h1 style={{ textAlign: "center" }}>Search Mobile Apps</h1>
      <ItemList
        data={data.filter((item) => {
          return (
            item.name.toLowerCase().indexOf(valueOfSearch.toLowerCase()) >= 0
          );
        })}
      />
    </>
  );
};

export default Search;
