import React, { useEffect, useContext } from "react";
import { MobileApsContext } from "../contexts/mobile-aps-context";
import ItemList from "./ItemList";

const Home = () => {
  const { data, api } = useContext(MobileApsContext);
  const { handleGetAll } = api;

  useEffect(() => {
    handleGetAll();
  }, []);

  return (
    <>
      <h1 style={{ textAlign: "center" }}>Popular Mobile Apps</h1>
      <ItemList data={data} />
    </>
  );
};

export default Home;
