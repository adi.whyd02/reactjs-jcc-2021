const Content = (props) => {
  return (
    <div className="row">
      <div className="section">
        <div className="card">{props.children}</div>
      </div>
    </div>
  );
};

export default Content;
