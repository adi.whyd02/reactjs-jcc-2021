const About = () => {
  return (
    <div style={{ border: "1px solid gray", padding: "15px" }}>
      <h1 style={{ textAlign: "center" }}>
        Data Peserta Jabarcodingcamp-2021 Reactjs
      </h1>
      <div>
        <ol type="1">
          <li>
            <b>Nama:</b> Adi Wahyudi
          </li>
          <li>
            <b>Email:</b> adi.whyd02@gmail.com
          </li>
          <li>
            <b>Sistem Operasi yang digunakan:</b> Windows 10
          </li>
          <li>
            <b>Akun Gitlab:</b>
            <a href="https://gitlab.com/adi.whyd02">adi.whyd02</a>
          </li>
          <li>
            <b>Akun Telegram:</b> <a href="https://t.me/diwahyud">diwahyud</a>
          </li>
        </ol>
      </div>
    </div>
  );
};

export default About;
