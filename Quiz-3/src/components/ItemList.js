import React from "react";
import { List, Rate } from "antd";

const ItemList = (props) => {
  const computePrice = (numb) => {
    if (numb === 0) return "Free";
    else if (!numb) return "Tidak didefinisikan";
    numb = parseInt(numb);
    const format = numb.toString().split("").reverse().join("");
    const convert = format.match(/\d{1,3}/g);
    const rupiah =
      "Rp. " + convert.join(".").split("").reverse().join("") + ",-";

    return rupiah;
  };

  const computeSize = (numb) => {
    if (!numb) return "Tidak didefinisikan";
    else if (numb < 1000) return numb + " MB";
    const convert = Math.round((numb / 1000) * 100) / 100;
    return convert + " GB";
  };
  return (
    <>
      <List
        itemLayout="vertical"
        size="large"
        pagination={{
          onChange: (page) => {
            console.log(page);
          },
          pageSize: 3,
        }}
        dataSource={props.data}
        renderItem={(item) => (
          <List.Item
            key={item.title}
            extra={<img width={272} alt="logo" src={item.image_url} />}
          >
            <List.Item.Meta title={item.name} />
            <div>
              <p style={{ fontSize: "9pt", color: "gray" }}>
                {item.description}
              </p>
              <div style={{ display: "flex", justifyContent: "start" }}>
                <p
                  style={{
                    fontSize: "9pt",
                    fontWeight: "bold",
                    marginRight: "10px",
                  }}
                >
                  Category:
                </p>
                <p style={{ fontSize: "9pt" }}>{item.category}</p>
              </div>
              <div style={{ display: "flex", justifyContent: "start" }}>
                <p
                  style={{
                    fontSize: "9pt",
                    fontWeight: "bold",
                    marginRight: "10px",
                  }}
                >
                  Price:
                </p>
                <p style={{ fontSize: "9pt" }}>{computePrice(item.price)}</p>
              </div>
              <div style={{ display: "flex", justifyContent: "start" }}>
                <p
                  style={{
                    fontSize: "9pt",
                    fontWeight: "bold",
                    marginRight: "10px",
                  }}
                >
                  Size:
                </p>
                <p style={{ fontSize: "9pt" }}>{computeSize(item.size)}</p>
              </div>
              <div style={{ display: "flex", justifyContent: "start" }}>
                <p
                  style={{
                    fontSize: "9pt",
                    fontWeight: "bold",
                    marginRight: "10px",
                  }}
                >
                  Platform:
                </p>
                <p style={{ fontSize: "9pt" }}>{item.platform}</p>
              </div>
              <div>
                <Rate allowHalf value={item.rating} />
              </div>
            </div>
          </List.Item>
        )}
      />
    </>
  );
};

export default ItemList;
