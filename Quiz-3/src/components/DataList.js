import React, { useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { MobileApsContext } from "../contexts/mobile-aps-context";
import { Table, Space, Button } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";

const DataList = () => {
  const { data, api } = useContext(MobileApsContext);
  const { handleGetAll, handleDelete } = api;

  const columns = [
    {
      title: "No",
      dataIndex: "no",
      key: "no",
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Category",
      dataIndex: "category",
      key: "category",
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "description",
    },
    {
      title: "Release Year",
      dataIndex: "release_year",
      key: "release_year",
    },
    {
      title: "Size",
      dataIndex: "size",
      key: "size",
    },
    {
      title: "Price",
      dataIndex: "price",
      key: "price",
    },
    {
      title: "Rating",
      dataIndex: "rating",
      key: "rating",
    },
    {
      title: "Platform",
      dataIndex: "platform",
      key: "platform",
    },
    {
      title: "Image Url",
      dataIndex: "image_url",
      key: "image_url",
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Space size="middle" key={text}>
          <Link to={`/mobile-form/edit/${record.id}`}>
            <Button style={{ background: "yellow" }}>
              <EditOutlined style={{ color: "black" }} />
            </Button>
          </Link>
          <Button type="danger" onClick={() => deleteItem(record.id)}>
            <DeleteOutlined />
          </Button>
        </Space>
      ),
    },
  ];

  const deleteItem = async (id) => {
    await handleDelete(id);
  };

  useEffect(() => {
    handleGetAll();
  }, []);

  return (
    <>
      <div className="card">
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "start",
          }}
        >
          <h1 className="card-title">Mobile Apps List</h1>
          <Link to="/mobile-form">
            <Button type="primary">Create new</Button>
          </Link>
        </div>
        <Table columns={columns} dataSource={data} rowKey="id" />
      </div>
    </>
  );
};

export default DataList;
