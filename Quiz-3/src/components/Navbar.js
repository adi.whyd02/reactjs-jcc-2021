import { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import logo from "../assets/img/logo.png";
const Navbar = () => {
  const [word, setWord] = useState("");
  const history = useHistory();

  const handleSubmit = (event) => {
    event.preventDefault();
    history.push(`/search/${word}`);
  };

  return (
    <div className="topnav">
      <Link to="/">
        <img src={logo} width="130" />
      </Link>
      <Link to="/">Home</Link>
      <Link to="/mobile-list">Movie List</Link>
      <Link to="/about">About</Link>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          value={word}
          onChange={(e) => setWord(e.target.value)}
        />
        <input type="submit" value="Cari" />
      </form>
    </div>
  );
};

export default Navbar;
