import { Switch, Route } from "react-router-dom";
import About from "../components/About";
import DataList from "../components/DataList";
import FormList from "../components/FormList";
import Home from "../components/Home";
import Search from "../components/Search";

const MainRoute = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact path="/mobile-list">
        <DataList />
      </Route>
      <Route exact path="/about">
        <About />
      </Route>
      <Route exact path="/mobile-form">
        <FormList />
      </Route>
      <Route exact path="/mobile-form/edit/:id">
        <FormList />
      </Route>
      <Route exact path="/search/:valueOfSearch">
        <Search />
      </Route>
    </Switch>
  );
};

export default MainRoute;
