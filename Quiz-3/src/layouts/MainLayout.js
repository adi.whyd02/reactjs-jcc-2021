import "../assets/css/style.css";
import { BrowserRouter as Router } from "react-router-dom";
import Content from "../components/Content";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import MainRoute from "../routes/MainRoute";
import { MobileApsProvider } from "../contexts/mobile-aps-context";

const MainLayout = (props) => {
  return (
    <Router>
      <Navbar />
      <Content>
        <MobileApsProvider>
          <MainRoute />
        </MobileApsProvider>
      </Content>
      <Footer />
    </Router>
  );
};

export default MainLayout;
