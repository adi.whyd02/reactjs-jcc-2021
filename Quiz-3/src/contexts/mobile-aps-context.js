import React, { createContext, useState } from "react";
import axios from "axios";
import { message } from "antd";

export const MobileApsContext = createContext();

export const MobileApsProvider = (props) => {
  const [data, setData] = useState([]);
  const [currentId, setCurrentId] = useState(null);

  const computePlatform = (is_android_app, is_ios_app) => {
    if (is_android_app && is_ios_app) return "Android & IOS";
    else if (is_android_app) return "Android";
    else if (is_ios_app) return "IOS";
    else return "Tidak ada platform";
  };

  const api = {
    handleGetAll: async () => {
      const result = await axios.get(
        `http://backendexample.sanbercloud.com/api/mobile-apps`
      );

      setData(
        result.data.map((item, index) => {
          return {
            no: index + 1,
            id: item.id,
            name: item.name,
            description: item.description,
            category: item.category,
            release_year: item.release_year,
            size: item.size,
            price: item.price,
            rating: item.rating,
            image_url: item.image_url,
            is_android_app: item.is_android_app,
            is_ios_app: item.is_ios_app,
            platform: computePlatform(item.is_android_app, item.is_ios_app),
          };
        })
      );

      return data;
    },
    handleCreate: (input) => {
      axios
        .post(`http://backendexample.sanbercloud.com/api/mobile-apps`, {
          ...input,
          is_android_app: input.platform.includes("Android"),
          is_ios_app: input.platform.includes("IOS"),
        })
        .then(() => {
          api.handleGetAll();
          setTimeout(() => {
            message.success("Data berhasil di tambahkan!");
          }, 1000);
        })
        .catch((error) =>
          message.error(`Data gagal ditambahkan, ${error.message}`)
        );
    },
    handleRead: async (id) => {
      const load = await axios.get(
        `http://backendexample.sanbercloud.com/api/mobile-apps/${id}`
      );
      return load.data;
    },
    handleUpdate: (input) => {
      axios
        .put(
          `http://backendexample.sanbercloud.com/api/mobile-apps/${currentId}`,
          {
            ...input,
            is_android_app: input.platform.includes("Android"),
            is_ios_app: input.platform.includes("IOS"),
          }
        )
        .then(() => {
          api.handleGetAll();
          setTimeout(() => {
            message.success("Data berhasil di update!");
          }, 1000);
        })
        .catch((error) =>
          message.error(`Data gagal di update, ${error.message}`)
        );
    },
    handleDelete: (id) => {
      axios
        .delete(`http://backendexample.sanbercloud.com/api/mobile-apps/${id}`)
        .then(() => {
          api.handleGetAll();
          message.success("Data berhasil di hapus!");
        })
        .catch((error) =>
          message.error(`Data gagal dihapus, ${error.message}`)
        );
    },
    handleSearch: async (valueOfSearch) => {
      //   console.log(data);
      if (api.handleGetAll()) {
        console.log("a");
        const search = data.filter((item) => {
          return item.name === valueOfSearch;
        });

        console.log(search);

        return search;
      }
    },
  };

  return (
    <MobileApsContext.Provider
      value={{
        data,
        setData,
        currentId,
        setCurrentId,
        api,
      }}
    >
      {props.children}
    </MobileApsContext.Provider>
  );
};
