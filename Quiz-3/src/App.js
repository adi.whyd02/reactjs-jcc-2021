import "antd/dist/antd.css";
import MainLayout from "./layouts/MainLayout";

function App() {
  return (
    <div className="App">
      <MainLayout />
    </div>
  );
}

export default App;
