import React, { createContext, useState } from "react";
import axios from "axios";
import { message } from "antd";

export const MahasiswaContext = createContext();

export const MahasiswaProvider = (props) => {
  const [daftarMahasiswa, setDaftarMahasiswa] = useState([]);
  const [currentId, setCurrentId] = useState(null);

  const computeIndeks = (score) => {
    if (score >= 80) return "A";
    else if (score >= 70 && score < 80) return "B";
    else if (score >= 60 && score < 70) return "C";
    else if (score >= 50 && score < 60) return "D";
    else if (score < 50) return "E";
    else return "Nilai tidak terindeks";
  };

  const api = {
    handleGetAll: async () => {
      const result = await axios.get(
        `http://backendexample.sanbercloud.com/api/student-scores`
      );

      setDaftarMahasiswa(
        result.data.map((item) => {
          return {
            id: item.id,
            name: item.name,
            course: item.course,
            score: item.score,
            indeks: computeIndeks(item.score),
          };
        })
      );
    },
    handleCreate: (input) => {
      axios
        .post(`http://backendexample.sanbercloud.com/api/student-scores`, input)
        .then((res) => {
          setDaftarMahasiswa([...daftarMahasiswa, res.data]);
          setTimeout(() => {
            message.success("Data berhasil di tambahkan!");
          }, 2000);
        })
        .catch((error) =>
          message.error(`Data gagal ditambahkan, ${error.message}`)
        );
    },
    handleRead: async (id) => {
      const load = await axios.get(
        `http://backendexample.sanbercloud.com/api/student-scores/${id}`
      );
      return load.data;
    },
    handleUpdate: (input) => {
      axios
        .put(
          `http://backendexample.sanbercloud.com/api/student-scores/${currentId}`,
          input
        )
        .then(() => {
          let data = daftarMahasiswa.map((el) => {
            if (el.id === currentId) {
              el = { id: currentId, ...input };
            }
            return el;
          });
          setDaftarMahasiswa(data);
          setTimeout(() => {
            message.success("Data berhasil di update!");
          }, 2000);
        })
        .catch((error) =>
          message.error(`Data gagal di update, ${error.message}`)
        );
    },
    handleDelete: (id) => {
      axios
        .delete(
          `http://backendexample.sanbercloud.com/api/student-scores/${id}`
        )
        .then(() => {
          const data = daftarMahasiswa.filter((el) => {
            return el.id !== id;
          });
          setDaftarMahasiswa(data);
          message.success("Data berhasil di hapus!");
        })
        .catch((error) =>
          message.error(`Data gagal dihapus, ${error.message}`)
        );
    },
  };

  return (
    <MahasiswaContext.Provider
      value={{
        daftarMahasiswa,
        setDaftarMahasiswa,
        currentId,
        setCurrentId,
        api,
      }}
    >
      {props.children}
    </MahasiswaContext.Provider>
  );
};
