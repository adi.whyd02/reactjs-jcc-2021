import React, { useEffect, useContext } from "react";
import { MahasiswaContext } from "./mahasiswa-context";

const MahasiswaList = () => {
  const { daftarMahasiswa, setCurrentId, api } = useContext(MahasiswaContext);
  const { handleGetAll, handleDelete } = api;

  const computeIndeks = (score) => {
    if (score >= 80) return "A";
    else if (score >= 70 && score < 80) return "B";
    else if (score >= 60 && score < 70) return "C";
    else if (score >= 50 && score < 60) return "D";
    else if (score < 50) return "E";
    else return "Nilai tidak terindeks";
  };

  useEffect(() => {
    handleGetAll();
  }, []);

  return (
    <>
      <div className="card">
        <h3 className="card-title">Daftar Nilai Mahasiswa</h3>
        <table>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Mata Kuliah</th>
              <th>Nilai</th>
              <th>Indeks</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            {daftarMahasiswa.map((val, index) => {
              return (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{val.name}</td>
                  <td>{val.course}</td>
                  <td>{val.score}</td>
                  <td>{computeIndeks(val.score)}</td>
                  <td>
                    <button
                      className="btn btn-warning"
                      onClick={() => setCurrentId(val.id)}
                    >
                      Edit
                    </button>
                    <button
                      className="btn btn-danger"
                      onClick={() => handleDelete(val.id)}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default MahasiswaList;
