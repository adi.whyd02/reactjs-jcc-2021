import React, { useState, useEffect, useContext } from "react";
import { MahasiswaContext } from "./mahasiswa-context";

const MahasiswaForm = () => {
  const { currentId, setCurrentId, api } = useContext(MahasiswaContext);
  const { handleRead, handleCreate, handleUpdate } = api;

  const [input, setInput] = useState({
    name: "",
    course: "",
    score: "",
  });
  const [errorMessage, setErrorMessage] = useState(false);

  const load = async () => {
    const data = await handleRead(currentId);
    setInput(data);
  };

  useEffect(() => {
    if (currentId) {
      load();
    }
  }, [currentId]);

  const handleChange = (event) => {
    let value = event.target.value;
    let name = event.target.name;
    setInput({ ...input, [name]: value });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    if (handleValidate()) {
      if (currentId === null) {
        handleCreate(input);
      } else {
        handleUpdate(input);
      }

      setInput({ name: "", course: "", score: "" });
      setCurrentId(null);
      setErrorMessage(false);
    }
  };

  const handleValidate = () => {
    if (!input.name || !input.course || !input.score) {
      setErrorMessage("Form harus diisi semua");
      return false;
    } else if (input.score < 0 || input.score > 100) {
      setErrorMessage("Nilai minimal 0 dan maksimal 100");
      return false;
    } else {
      return true;
    }
  };

  return (
    <div className="card">
      <h3 className="card-title">Form Nilai Mahasiswa</h3>
      {errorMessage && (
        <div className="error-message flex-between">
          <p>{errorMessage}</p>
          <p
            onClick={() => setErrorMessage(false)}
            style={{ cursor: "pointer" }}
          >
            X
          </p>
        </div>
      )}

      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <div>
            <label htmlFor="name">Nama:</label>
          </div>
          <input
            type="text"
            name="name"
            id="name"
            value={input.name}
            onChange={handleChange}
          />
        </div>

        <div className="form-group">
          <div>
            <label htmlFor="course">Mata Kuliah:</label>
          </div>
          <input
            type="text"
            name="course"
            id="course"
            value={input.course}
            onChange={handleChange}
          />
        </div>

        <div className="form-group">
          <div>
            <label htmlFor="score">Nilai:</label>
          </div>
          <input
            type="number"
            name="score"
            id="score"
            value={input.score}
            onChange={handleChange}
          />
        </div>
        <div className="form-group flex-end">
          <button className="btn btn-primary">submit</button>
        </div>
      </form>
    </div>
  );
};

export default MahasiswaForm;
