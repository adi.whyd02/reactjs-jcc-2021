import "../assets/css/crud.css";
import { MahasiswaProvider } from "./mahasiswa-context";
import MahasiswaForm from "./MahasiswaForm";
import MahasiswaList from "./MahasiswaList";

const MahasiswaMaster = () => {
  return (
    <MahasiswaProvider>
      <MahasiswaList />
      <MahasiswaForm />
    </MahasiswaProvider>
  );
};

export default MahasiswaMaster;
