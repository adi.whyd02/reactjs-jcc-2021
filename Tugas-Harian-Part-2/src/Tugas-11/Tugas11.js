import React, { useState } from "react";
import "../assets/css/crud.css";

const Crud = () => {
  const [daftarBuah, setDaftarBuah] = useState([
    { nama: "Nanas", hargaTotal: 100000, beratTotal: 4000 },
    { nama: "Manggis", hargaTotal: 350000, beratTotal: 10000 },
    { nama: "Nangka", hargaTotal: 90000, beratTotal: 2000 },
    { nama: "Durian", hargaTotal: 400000, beratTotal: 5000 },
    { nama: "Strawberry", hargaTotal: 120000, beratTotal: 6000 },
  ]);
  const [currentIndex, setCurrentIndex] = useState(-1);

  const [nama, setNama] = useState("");
  const [hargaTotal, setHargaTotal] = useState("");
  const [beratTotal, setBeratTotal] = useState("");

  const [errorMessage, setErrorMessage] = useState(false);

  const handleDelete = (event) => {
    let index = parseInt(event.target.value);
    let deletedItem = daftarBuah[index];
    let newData = daftarBuah.filter((e) => {
      return e !== deletedItem;
    });
    setDaftarBuah(newData);
  };

  const handleEdit = (event) => {
    let index = parseInt(event.target.value);
    let editValue = daftarBuah[index];
    setNama(editValue.nama);
    setHargaTotal(editValue.hargaTotal);
    setBeratTotal(editValue.beratTotal);
    setCurrentIndex(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    if (handleValidate()) {
      let newData = daftarBuah;

      if (currentIndex === -1) {
        newData = [
          ...daftarBuah,
          {
            nama,
            hargaTotal,
            beratTotal,
          },
        ];
      } else {
        newData[currentIndex] = {
          nama,
          hargaTotal,
          beratTotal,
        };
      }
      setDaftarBuah(newData);
      setNama("");
      setHargaTotal("");
      setBeratTotal("");
      setCurrentIndex(-1);
      setErrorMessage(false);
    }
  };

  const handleValidate = () => {
    if (!nama || !hargaTotal || !beratTotal) {
      setErrorMessage("Form harus diisi semua");
      return false;
    } else if (beratTotal < 2000) {
      setErrorMessage("Berat total minimal 2000 gram");
      return false;
    } else {
      return true;
    }
  };

  return (
    <>
      <div className="card">
        <h3 className="card-title">Daftar Harga Buah</h3>
        <table>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Harga total</th>
              <th>Berat total</th>
              <th>Harga per kg</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            {daftarBuah.map((val, index) => {
              return (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{val.nama}</td>
                  <td>{val.hargaTotal}</td>
                  <td>{val.beratTotal / 1000} kg</td>
                  <td>{val.hargaTotal / (val.beratTotal / 1000)}</td>
                  <td>
                    <button
                      className="btn btn-warning"
                      onClick={handleEdit}
                      value={index}
                    >
                      Edit
                    </button>
                    <button
                      className="btn btn-danger"
                      onClick={handleDelete}
                      value={index}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>

      <div className="card">
        <h3 className="card-title">Form Daftar Buah</h3>
        {errorMessage && (
          <div className="error-message flex-between">
            <p>{errorMessage}</p>
            <p
              onClick={() => setErrorMessage(false)}
              style={{ cursor: "pointer" }}
            >
              X
            </p>
          </div>
        )}

        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <div>
              <label htmlFor="nama">Nama:</label>
            </div>
            <input
              type="text"
              value={nama}
              name="nama"
              id="nama"
              value={nama}
              onChange={(e) => setNama(e.target.value)}
            />
          </div>

          <div className="form-group">
            <div>
              <label htmlFor="hargaTotal">Harga Total:</label>
            </div>
            <input
              type="number"
              value={hargaTotal}
              name="hargaTotal"
              id="hargaTotal"
              value={hargaTotal}
              onChange={(e) => setHargaTotal(e.target.value)}
            />
          </div>

          <div className="form-group">
            <div>
              <label htmlFor="beratTotal">Berat Total (dalam gram):</label>
            </div>
            <input
              type="number"
              value={beratTotal}
              name="beratTotal"
              id="beratTotal"
              value={beratTotal}
              onChange={(e) => setBeratTotal(e.target.value)}
            />
          </div>
          <div className="form-group flex-end">
            <button className="btn btn-primary">submit</button>
          </div>
        </form>
      </div>
    </>
  );
};

export default Crud;
