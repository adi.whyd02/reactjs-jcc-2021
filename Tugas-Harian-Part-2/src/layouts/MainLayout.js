import { Layout } from "antd";
import Nav from "../Tugas-14/Nav";

const { Header, Footer, Content } = Layout;

const MainLayout = (props) => {
  return (
    <Layout className="layout">
      <Nav />
      <Content style={{ padding: "0 50px" }}>{props.children}</Content>
      <Footer style={{ textAlign: "center" }}>
        JCC ©2021 Created by Adi Wahyudi
      </Footer>
    </Layout>
  );
};

export default MainLayout;
