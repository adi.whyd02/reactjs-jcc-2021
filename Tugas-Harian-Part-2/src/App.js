import Tugas9 from "./Tugas-9/Tugas9";
import Tugas10 from "./Tugas-10/Tugas10";
import Tugas11 from "./Tugas-11/Tugas11";
import Tugas12 from "./Tugas-12/Tugas12";
import Tugas13 from "./Tugas-13/Tugas13";
import Routes from "./Tugas-14/Routes";
import "antd/dist/antd.css";

const App = () => {
  return (
    <>
      {/* <Tugas10 />
      <Tugas9 /> */}
      {/* <Tugas11 /> */}
      {/* <Tugas12 /> */}
      {/* <Tugas13 /> */}
      <Routes />
    </>
  );
};

export default App;
