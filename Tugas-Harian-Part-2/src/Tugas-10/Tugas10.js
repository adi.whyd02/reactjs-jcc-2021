import React, { useState, useEffect } from "react";
import "../assets/css/time.css";

const Time = () => {
  const [date, setDate] = useState(new Date());
  const [timeLeft, setTimeLeft] = useState(100);
  const [isFinished, setIsFinished] = useState(false);

  useEffect(() => {
    let timer = setInterval(() => setDate(new Date()), 1000);
    return () => {
      clearInterval(timer);
    };
  });

  useEffect(() => {
    if (!timeLeft) {
      setIsFinished(true);
      return;
    }

    const interval = setInterval(() => {
      setTimeLeft(timeLeft - 1);
    }, 1000);

    return () => clearInterval(interval);
  }, [timeLeft]);

  return (
    <div className="container-time">
      {!isFinished && (
        <>
          <h1>Now At : {date.toLocaleTimeString()}</h1>
          <p>Countdown: {timeLeft}</p>
        </>
      )}
    </div>
  );
};

export default Time;
