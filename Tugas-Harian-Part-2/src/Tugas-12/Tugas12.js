import React, { useState, useEffect } from "react";
import "../assets/css/crud.css";
import axios from "axios";

const Crud = () => {
  const [daftarMahasiswa, setDaftarMahasiswa] = useState([]);
  const [currentId, setCurrentId] = useState(null);

  const [input, setInput] = useState({
    name: "",
    course: "",
    score: "",
  });

  const [errorMessage, setErrorMessage] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get(
        `http://backendexample.sanbercloud.com/api/student-scores`
      );

      setDaftarMahasiswa(
        result.data.map((item) => {
          return {
            id: item.id,
            name: item.name,
            course: item.course,
            score: item.score,
          };
        })
      );
    };

    fetchData();
  }, []);

  const computeIndeks = (score) => {
    if (score >= 80) return "A";
    else if (score >= 70 && score < 80) return "B";
    else if (score >= 60 && score < 70) return "C";
    else if (score >= 50 && score < 60) return "D";
    else if (score < 50) return "E";
    else return "Nilai tidak terindeks";
  };

  const handleDelete = (event) => {
    let id = parseInt(event.target.value);
    axios
      .delete(`http://backendexample.sanbercloud.com/api/student-scores/${id}`)
      .then(() => {
        const data = daftarMahasiswa.filter((el) => {
          return el.id !== id;
        });
        setDaftarMahasiswa(data);
      });
  };

  const handleEdit = (event) => {
    let id = parseInt(event.target.value);
    axios
      .get(`http://backendexample.sanbercloud.com/api/student-scores/${id}`)
      .then((res) => {
        setInput(res.data);
        setCurrentId(id);
      });
  };

  const handleChange = (event) => {
    let value = event.target.value;
    let name = event.target.name;
    setInput({ ...input, [name]: value });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    if (handleValidate()) {
      if (currentId === null) {
        axios
          .post(
            `http://backendexample.sanbercloud.com/api/student-scores`,
            input
          )
          .then((res) => {
            setDaftarMahasiswa([...daftarMahasiswa, res.data]);
          });
      } else {
        axios
          .put(
            `http://backendexample.sanbercloud.com/api/student-scores/${currentId}`,
            input
          )
          .then(() => {
            let data = daftarMahasiswa.map((el) => {
              if (el.id === currentId) {
                el = { id: currentId, ...input };
              }
              return el;
            });
            setDaftarMahasiswa(data);
          });
      }

      setInput({ name: "", course: "", score: "" });
      setCurrentId(null);
      setErrorMessage(false);
    }
  };

  const handleValidate = () => {
    if (!input.name || !input.course || !input.score) {
      setErrorMessage("Form harus diisi semua");
      return false;
    } else if (input.score < 0 || input.score > 100) {
      setErrorMessage("Nilai minimal 0 dan maksimal 100");
      return false;
    } else {
      return true;
    }
  };

  return (
    <>
      <div className="card">
        <h3 className="card-title">Daftar Nilai Mahasiswa</h3>
        <table>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Mata Kuliah</th>
              <th>Nilai</th>
              <th>Indeks</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            {daftarMahasiswa.map((val, index) => {
              return (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{val.name}</td>
                  <td>{val.course}</td>
                  <td>{val.score}</td>
                  <td>{computeIndeks(val.score)}</td>
                  <td>
                    <button
                      className="btn btn-warning"
                      onClick={handleEdit}
                      value={val.id}
                    >
                      Edit
                    </button>
                    <button
                      className="btn btn-danger"
                      onClick={handleDelete}
                      value={val.id}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>

      <div className="card">
        <h3 className="card-title">Form Nilai Mahasiswa</h3>
        {errorMessage && (
          <div className="error-message flex-between">
            <p>{errorMessage}</p>
            <p
              onClick={() => setErrorMessage(false)}
              style={{ cursor: "pointer" }}
            >
              X
            </p>
          </div>
        )}

        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <div>
              <label htmlFor="name">Nama:</label>
            </div>
            <input
              type="text"
              name="name"
              id="name"
              value={input.name}
              onChange={handleChange}
            />
          </div>

          <div className="form-group">
            <div>
              <label htmlFor="course">Mata Kuliah:</label>
            </div>
            <input
              type="text"
              name="course"
              id="course"
              value={input.course}
              onChange={handleChange}
            />
          </div>

          <div className="form-group">
            <div>
              <label htmlFor="score">Nilai:</label>
            </div>
            <input
              type="number"
              name="score"
              id="score"
              value={input.score}
              onChange={handleChange}
            />
          </div>
          <div className="form-group flex-end">
            <button className="btn btn-primary">submit</button>
          </div>
        </form>
      </div>
    </>
  );
};

export default Crud;
