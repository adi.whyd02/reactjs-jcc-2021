import React, { useState, useEffect, useContext } from "react";
import { MahasiswaContext } from "../Tugas-13/mahasiswa-context";
import { Link, useHistory, useParams } from "react-router-dom";

const MahasiswaForm = () => {
  const { currentId, setCurrentId, api } = useContext(MahasiswaContext);
  const [isLoading, setIsLoading] = useState(false);
  const { handleGetAll, handleRead, handleCreate, handleUpdate } = api;
  const history = useHistory();
  const { id } = useParams();

  const [input, setInput] = useState({
    name: "",
    course: "",
    score: "",
  });
  const [errorMessage, setErrorMessage] = useState(false);

  const load = async () => {
    const data = await handleRead(id);
    setCurrentId(id);
    setInput(data);
  };

  useEffect(() => {
    if (id) {
      setIsLoading(true);
      load();
      setIsLoading(false);
    }

    return () => {
      setCurrentId(null);
    };
  }, [id]);

  const handleChange = (event) => {
    let value = event.target.value;
    let name = event.target.name;
    setInput({ ...input, [name]: value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    setIsLoading(true);
    if (handleValidate()) {
      if (currentId === null) {
        await handleCreate(input);
      } else {
        await handleUpdate(input);
      }

      await handleGetAll();
      history.push("/tugas15");
    }
    setIsLoading(false);
  };

  const handleValidate = () => {
    if (!input.name || !input.course || !input.score) {
      setErrorMessage("Form harus diisi semua");
      return false;
    } else if (input.score < 0 || input.score > 100) {
      setErrorMessage("Nilai minimal 0 dan maksimal 100");
      return false;
    } else {
      return true;
    }
  };

  return (
    <div className="card">
      <h3 className="card-title">Form Nilai Mahasiswa</h3>
      {errorMessage && (
        <div className="error-message flex-between">
          <p>{errorMessage}</p>
          <p
            onClick={() => setErrorMessage(false)}
            style={{ cursor: "pointer" }}
          >
            X
          </p>
        </div>
      )}

      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <div>
            <label htmlFor="name">Nama:</label>
          </div>
          <input
            type="text"
            name="name"
            id="name"
            value={input.name}
            onChange={handleChange}
          />
        </div>

        <div className="form-group">
          <div>
            <label htmlFor="course">Mata Kuliah:</label>
          </div>
          <input
            type="text"
            name="course"
            id="course"
            value={input.course}
            onChange={handleChange}
          />
        </div>

        <div className="form-group">
          <div>
            <label htmlFor="score">Nilai:</label>
          </div>
          <input
            type="number"
            name="score"
            id="score"
            value={input.score}
            onChange={handleChange}
          />
        </div>
        <div className="form-group flex-end">
          <button className="btn btn-primary">
            {isLoading ? "Loading.." : "Submit"}
          </button>
        </div>
      </form>

      <Link to="/tugas15">Back to table</Link>
    </div>
  );
};

export default MahasiswaForm;
