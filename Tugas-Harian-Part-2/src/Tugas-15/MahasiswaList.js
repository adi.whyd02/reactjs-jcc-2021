import React, { useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { MahasiswaContext } from "../Tugas-13/mahasiswa-context";
import { Table, Space, Button } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";

const MahasiswaList = () => {
  const { daftarMahasiswa, api } = useContext(MahasiswaContext);
  const { handleGetAll, handleDelete } = api;

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Course",
      dataIndex: "course",
      key: "course",
    },
    {
      title: "Score",
      dataIndex: "score",
      key: "score",
    },
    {
      title: "Indeks Score",
      key: "indeks",
      dataIndex: "indeks",
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Space size="middle" key={text}>
          <Link to={`/tugas15/edit/${record.id}`}>
            <Button style={{ background: "yellow" }}>
              <EditOutlined style={{ color: "white" }} />
            </Button>
          </Link>
          <Button type="danger" onClick={() => deleteItem(record.id)}>
            <DeleteOutlined />
          </Button>
        </Space>
      ),
    },
  ];

  const deleteItem = async (id) => {
    await handleDelete(id);
  };

  useEffect(() => {
    handleGetAll();
  }, []);

  return (
    <>
      <div className="card">
        <div className="flex-between" style={{ alignItems: "start" }}>
          <h3 className="card-title">Daftar Nilai Mahasiswa</h3>
          <Link to="/tugas15/create">
            <Button type="primary">Create new</Button>
          </Link>
        </div>
        <Table columns={columns} dataSource={daftarMahasiswa} rowKey="id" />
      </div>
    </>
  );
};

export default MahasiswaList;
