import React, { useState } from "react";
import ThingsToDoItem from "./ThingsToDoItem";

import "../assets/css/things-to-do.css";
import logo from "../assets/img/logo-jcc.png";

const ThingsToDo = () => {
  const [items, setItems] = useState([
    { id: 1, title: "Belajar Git & CLI", status: true },
    { id: 2, title: "Belajar HTML & CSS", status: false },
    { id: 3, title: "Belajar Javascript", status: false },
    { id: 4, title: "Belajar ReactJs Dasar", status: false },
    { id: 5, title: "Belajar ReactJs Advance", status: false },
  ]);

  return (
    <div className="container">
      <div className="card">
        <div className="card-header">
          <img src={logo} alt="logo-jabar-coding-camp" />
          <h1>THINGS TO DO</h1>
          <p>During Bootcamp in Jabarcodingcamp</p>
        </div>
        <div>
          {items.map((item) => (
            <ThingsToDoItem key={item.id} item={item} />
          ))}
        </div>
        <div>
          <button className="btn-send">Send</button>
        </div>
      </div>
    </div>
  );
};

export default ThingsToDo;
