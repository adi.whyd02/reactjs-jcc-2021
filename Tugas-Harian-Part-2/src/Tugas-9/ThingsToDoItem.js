const ThingsToDoItem = (props) => {
  let { item } = props;

  return (
    <div>
      <div className="item-todo">
        <input type="checkbox" id={item.id} />
        <label htmlFor={item.id}>{item.title}</label>
      </div>
    </div>
  );
};

export default ThingsToDoItem;
