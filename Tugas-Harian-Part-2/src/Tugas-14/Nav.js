import { useContext } from "react";
import { Link, useLocation } from "react-router-dom";
import { ThemeContext } from "./theme-context";
import BtnChangeTheme from "./BtnChangeTheme";

import { Menu } from "antd";

const Nav = () => {
  const { theme } = useContext(ThemeContext);
  const location = useLocation();

  return (
    <>
      <Menu
        theme={theme}
        mode="horizontal"
        defaultSelectedKeys={location.pathname}
      >
        <Menu.Item key="/">
          <Link to="/" className={theme}>
            Tugas 9
          </Link>
        </Menu.Item>
        <Menu.Item key="/tugas10">
          <Link to="/tugas10" className={theme}>
            Tugas 10
          </Link>
        </Menu.Item>
        <Menu.Item key="/tugas11">
          <Link to="/tugas11" className={theme}>
            Tugas 11
          </Link>
        </Menu.Item>
        <Menu.Item key="/tugas12">
          <Link to="/tugas12" className={theme}>
            Tugas 12
          </Link>
        </Menu.Item>
        <Menu.Item key="/tugas13">
          <Link to="/tugas13" className={theme}>
            Tugas 13
          </Link>
        </Menu.Item>
        <Menu.Item key="/tugas14">
          <Link to="/tugas14" className={theme}>
            Tugas 14
          </Link>
        </Menu.Item>
        <Menu.Item key="/tugas15">
          <Link to="/tugas15" className={theme}>
            Tugas 15
          </Link>
        </Menu.Item>
      </Menu>
      <div style={{ position: "absolute", top: 10, right: 10 }}>
        <BtnChangeTheme />
      </div>
    </>
  );
};

export default Nav;
