import { useContext } from "react";
import { ThemeContext } from "./theme-context";
import { Switch } from "antd";

const BtnChangeTheme = () => {
  const { theme, setTheme } = useContext(ThemeContext);
  return (
    <Switch
      onClick={setTheme}
      checkedChildren="Dark"
      unCheckedChildren="Dark"
      checked={theme === "light" ? false : true}
    />
  );
};

export default BtnChangeTheme;
