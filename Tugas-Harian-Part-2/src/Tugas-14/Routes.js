import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Tugas9 from "../Tugas-9/Tugas9";
import Tugas10 from "../Tugas-10/Tugas10";
import Tugas11 from "../Tugas-11/Tugas11";
import Tugas12 from "../Tugas-12/Tugas12";
import Tugas13 from "../Tugas-13/Tugas13";
import MahasiswaForm14 from "./MahasiswaForm";
import MahasiswaList14 from "./MahasiswaList";
import MahasiswaForm15 from "../Tugas-15/MahasiswaForm";
import MahasiswaList15 from "../Tugas-15/MahasiswaList";
import { MahasiswaProvider } from "../Tugas-13/mahasiswa-context";
import { ThemeProvider } from "./theme-context";
import MainLayout from "../layouts/MainLayout";

const Routes = () => {
  return (
    <ThemeProvider>
      <Router>
        <MainLayout>
          <Switch>
            <Route exact path="/">
              <Tugas9 />
            </Route>
            <Route path="/tugas10">
              <Tugas10 />
            </Route>
            <Route path="/tugas11">
              <Tugas11 />
            </Route>
            <Route path="/tugas12">
              <Tugas12 />
            </Route>
            <Route path="/tugas13">
              <Tugas13 />
            </Route>
            <MahasiswaProvider>
              <Route exact path="/tugas14">
                <MahasiswaList14 />
              </Route>
              <Route path="/tugas14/create">
                <MahasiswaForm14 />
              </Route>
              <Route path="/tugas14/edit/:id">
                <MahasiswaForm14 />
              </Route>
              <Route exact path="/tugas15">
                <MahasiswaList15 />
              </Route>
              <Route path="/tugas15/create">
                <MahasiswaForm15 />
              </Route>
              <Route path="/tugas15/edit/:id">
                <MahasiswaForm15 />
              </Route>
            </MahasiswaProvider>
          </Switch>
        </MainLayout>
      </Router>
    </ThemeProvider>
  );
};

export default Routes;
