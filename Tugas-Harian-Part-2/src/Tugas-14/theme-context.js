import { useState, createContext } from "react";

export const ThemeContext = createContext();

export const ThemeProvider = (props) => {
  const [theme, setTheme] = useState("light");

  return (
    <ThemeContext.Provider
      value={{
        theme,
        setTheme: () => setTheme(theme === "dark" ? "light" : "dark"),
      }}
    >
      {props.children}
    </ThemeContext.Provider>
  );
};
