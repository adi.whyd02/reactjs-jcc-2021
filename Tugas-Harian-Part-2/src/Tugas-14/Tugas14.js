import "../assets/css/crud.css";
import { MahasiswaProvider } from "../Tugas-13/mahasiswa-context";
import BtnChangeTheme from "./BtnChangeTheme";
import MahasiswaList from "./MahasiswaList";

const MahasiswaMaster = () => {
  return (
    <div>
      <BtnChangeTheme />
      <MahasiswaProvider>
        <MahasiswaList />
      </MahasiswaProvider>
    </div>
  );
};

export default MahasiswaMaster;
